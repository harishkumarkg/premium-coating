package com.ifive.premiumsfa.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ifive.premiumsfa.CompanyList;
import com.ifive.premiumsfa.R;
import com.ifive.premiumsfa.datas.models.responses.CompanyListResponse;
import com.ifive.premiumsfa.engine.PremiumEngine;

import java.util.List;

/**
 * Created by Syss4 on 2/8/2018.
 */

public class Company_Adapter extends RecyclerView.Adapter<Company_Adapter.MyViewHolder> {

    private Context context;
    private List<CompanyListResponse> salesListResponses;
    private CompanyList companyList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView compName,compId;
        private LinearLayout linearLayout;
        public MyViewHolder(View view) {
            super(view);
            compId = view.findViewById(R.id.cId);
            compName = view.findViewById(R.id.cName);
        }
    }

    public Company_Adapter(Context context, List<CompanyListResponse> companyListResponses, CompanyList companyList) {
        this.context = context;
        this.salesListResponses = companyListResponses;
        this.companyList = companyList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.company_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        final CompanyListResponse item = salesListResponses.get(position);
        holder.compId.setText(item.getCompanyId());
        holder.compName.setText(item.getName());

    }

    @Override
    public int getItemCount() {
        return salesListResponses.size();
    }
}
