package com.ifive.premiumsfa.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ifive.premiumsfa.Movement;
import com.ifive.premiumsfa.R;
import com.ifive.premiumsfa.datas.models.responses.ProductList;

import java.util.List;

/**
 * Created by Syss4 on 2/14/2018.
 */

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.MyViewHolder> {
    private Context context;
    private List<ProductList> cartList;
    private Movement salesOrderFragment;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout viewBackground, viewForeground;
        public TextView productName,proPrice, proQty, proGst,proTotal,serialNumber;
        public MyViewHolder(View view) {
            super(view);
            productName = view.findViewById(R.id.product_name);
            proPrice = view.findViewById(R.id.pro_price);
            proQty = view.findViewById(R.id.pro_qty);
            proGst = view.findViewById(R.id.pro_gst);
            proTotal = view.findViewById(R.id.pro_total);
            serialNumber = view.findViewById(R.id.serial_number);
            viewBackground = view.findViewById(R.id.view_background);
            viewForeground = view.findViewById(R.id.view_foreground);
        }
    }

    public ProductListAdapter(Context context, List<ProductList> cartList, Movement movement) {
        this.context = context;
        this.cartList = cartList;
        this.salesOrderFragment = salesOrderFragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sales_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final ProductList item = cartList.get(position);
        holder.productName.setText(item.getConcatSegment()+"("+item.getHsnCode()+")");
        holder.proPrice.setText(item.getPrice());
       /* holder.proQty.setText(item.getClassName());
        holder.proGst.setText(item.getTon()+"");
        holder.proTotal.setText(item.getTotal()+"");
        holder.serialNumber.setText(position+1+"");*/
       /* holder.viewForeground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                salesOrderFragment.editItem(position);
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return cartList.size();
    }

    public void removeItem(int position) {
        cartList.remove(position);
       // salesOrderFragment.calculateTotalAmount();
        notifyItemRemoved(position);
    }

    public void restoreItem(ProductList item, int position) {
        cartList.add(position, item);
        // notify item added by position
        notifyItemInserted(position);
    }
}
