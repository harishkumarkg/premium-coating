package com.ifive.premiumsfa.datas.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Comp11 on 1/25/2018.
 */

public class LoginResponse {
    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("image_name")
    @Expose
    private String imageName;
    @SerializedName("company_id")
    @Expose
    private String companyId;
    @SerializedName("remember_token")
    @Expose
    private String rememberToken;
    @SerializedName("forwarded_username")
    @Expose
    private String forwardedUsername;
    @SerializedName("forwarded_id")
    @Expose
    private String forwardedId;
    @SerializedName("ss_defaultloc_id")
    @Expose
    private String ssDefaultlocId;
    @SerializedName("ss_defaultloc_code")
    @Expose
    private String ssDefaultlocCode;
    @SerializedName("ss_defaultorg_id")
    @Expose
    private String ssDefaultorgId;
    @SerializedName("ss_defaultorg_name")
    @Expose
    private String ssDefaultorgName;
    @SerializedName("ss_defaultloc_name")
    @Expose
    private String ssDefaultlocName;
    @SerializedName("ss_defaultorg_type")
    @Expose
    private String ssDefaultorgType;
    @SerializedName("ss_defaultdep_id")
    @Expose
    private String ssDefaultdepId;
    @SerializedName("emp_id")
    @Expose
    private Integer empId;

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getRememberToken() {
        return rememberToken;
    }

    public void setRememberToken(String rememberToken) {
        this.rememberToken = rememberToken;
    }

    public String getForwardedUsername() {
        return forwardedUsername;
    }

    public void setForwardedUsername(String forwardedUsername) {
        this.forwardedUsername = forwardedUsername;
    }

    public String getForwardedId() {
        return forwardedId;
    }

    public void setForwardedId(String forwardedId) {
        this.forwardedId = forwardedId;
    }

    public String getSsDefaultlocId() {
        return ssDefaultlocId;
    }

    public void setSsDefaultlocId(String ssDefaultlocId) {
        this.ssDefaultlocId = ssDefaultlocId;
    }

    public String getSsDefaultlocCode() {
        return ssDefaultlocCode;
    }

    public void setSsDefaultlocCode(String ssDefaultlocCode) {
        this.ssDefaultlocCode = ssDefaultlocCode;
    }

    public String getSsDefaultorgId() {
        return ssDefaultorgId;
    }

    public void setSsDefaultorgId(String ssDefaultorgId) {
        this.ssDefaultorgId = ssDefaultorgId;
    }

    public String getSsDefaultorgName() {
        return ssDefaultorgName;
    }

    public void setSsDefaultorgName(String ssDefaultorgName) {
        this.ssDefaultorgName = ssDefaultorgName;
    }

    public String getSsDefaultlocName() {
        return ssDefaultlocName;
    }

    public void setSsDefaultlocName(String ssDefaultlocName) {
        this.ssDefaultlocName = ssDefaultlocName;
    }

    public String getSsDefaultorgType() {
        return ssDefaultorgType;
    }

    public void setSsDefaultorgType(String ssDefaultorgType) {
        this.ssDefaultorgType = ssDefaultorgType;
    }

    public String getSsDefaultdepId() {
        return ssDefaultdepId;
    }

    public void setSsDefaultdepId(String ssDefaultdepId) {
        this.ssDefaultdepId = ssDefaultdepId;
    }

    public Integer getEmpId() {
        return empId;
    }

    public void setEmpId(Integer empId) {
        this.empId = empId;
    }

}



