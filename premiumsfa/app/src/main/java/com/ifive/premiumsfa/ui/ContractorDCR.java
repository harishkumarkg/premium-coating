package com.ifive.premiumsfa.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ifive.premiumsfa.MainActivity;
import com.ifive.premiumsfa.Movement;
import com.ifive.premiumsfa.R;
import com.ifive.premiumsfa.adapter.spinner_adapter.OutLetSpinner;
import com.ifive.premiumsfa.adapter.spinner_adapter.ProductListSpinnerAdapter;
import com.ifive.premiumsfa.datas.SessionManager;
import com.ifive.premiumsfa.datas.models.requests.DcrRequest;
import com.ifive.premiumsfa.datas.models.requests.ProductsRequests;
import com.ifive.premiumsfa.datas.models.responses.DcrResponses;
import com.ifive.premiumsfa.datas.models.responses.OutletList;
import com.ifive.premiumsfa.datas.models.responses.ProductList;
import com.ifive.premiumsfa.datas.remote.UserAPICall;
import com.ifive.premiumsfa.engine.PremiumEngine;
import com.ifive.premiumsfa.engine.RetroFitEngine;
import com.ifive.premiumsfa.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Syss4 on 2/16/2018.
 */

public class ContractorDCR extends BaseActivity {

    ProgressDialog pDialog;
    @BindView(R.id.contractNmae)
    Spinner contName;
    @BindView(R.id.producttNmae)
    Spinner proName;
    @BindView(R.id.contName)
    TextView cName;
    @BindView(R.id.compName)
    TextView comName;
    @BindView(R.id.compAddress)
    TextView comAddress;
    @BindView(R.id.compPhone)
    TextView comPhones;
    @BindView(R.id.dcrRemarks)
    EditText enterRemark;
    @BindView(R.id.dcrSubmit)
    Button submit;
    @BindView(R.id.dcr_type)
    Spinner dcrType;
    private OutLetSpinner outLetSpinner;
    ProductsRequests poductList;
    private ProductListSpinnerAdapter productListSpinnerAdapter;
    DcrRequest dcrRequest;
    DcrResponses dcrResponses;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dcr);
        pDialog = PremiumEngine.myInstance.getProgDialog(this);
        ButterKnife.bind(this);
        getDatasList();
        getDatasLists();
    }

    private void setCustomerSpinner() {
        outLetSpinner = new OutLetSpinner(this,
                android.R.layout.simple_spinner_item,
                PremiumEngine.outletLists);
        contName.setAdapter(outLetSpinner);
        contName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                // Here you get the current item (a User object) that is selected by its position
                PremiumEngine.myInstance.itemSelecteds = outLetSpinner.getItem(position);
                cName.setText(outLetSpinner.getItem(position).getCompanyAdditionalInfo());
                comName.setText(outLetSpinner.getItem(position).getAddress1());
                comAddress.setText(outLetSpinner.getItem(position).getArea());
                comAddress.setText(outLetSpinner.getItem(position).getPanNo());
                comPhones.setText(String.valueOf(outLetSpinner.getItem(position).getCustomerId()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });
    }

    private void setProductSpinner() {
        productListSpinnerAdapter = new ProductListSpinnerAdapter(this,
                android.R.layout.simple_spinner_item,
                PremiumEngine.productList);
        proName.setAdapter(productListSpinnerAdapter);
        proName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                // Here you get the current item (a User object) that is selected by its position
                PremiumEngine.myInstance.itemSelected = productListSpinnerAdapter.getItem(position);
                /*hsnCode.setText(productListSpinnerAdapter.getItem(position).getHsnCode());
                classSpinner.setText(productListSpinnerAdapter.getItem(position).getPrice());
                uom.setText(productListSpinnerAdapter.getItem(position).getUomCodeId());*/
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });
    }

    public void getDatasLists() {
        pDialog.show();
        PremiumEngine.myInstance.productList = new ArrayList<>();
        UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);
        ProductsRequests productsRequests = new ProductsRequests();
        productsRequests.setCompanyId(11);


        Call<List<ProductList>> callEnqueue = userAPICall.product(/*sessionManager.getToken(this)*/"AceNKi4Go8hZ4lLmnS8659VOzY5pgUTyMoEFDU5i", productsRequests);
        callEnqueue.enqueue(new Callback<List<ProductList>>() {
            @Override
            public void onResponse(Call<List<ProductList>> call, Response<List<ProductList>> response) {
                PremiumEngine.myInstance.productList = response.body();

                if (PremiumEngine.myInstance.productList != null) {
                    Toast.makeText(ContractorDCR.this,
                            "Ready to Process", Toast.LENGTH_SHORT).show();
                    setProductSpinner();
                }
                pDialog.dismiss();
            }

            @Override
            public void onFailure(Call<List<ProductList>> call, Throwable t) {
                Toast.makeText(ContractorDCR.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                pDialog.dismiss();
            }
        });
    }

    public void getDatasList() {
        pDialog.show();
        PremiumEngine.myInstance.outletLists = new ArrayList<>();
        UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);
        ProductsRequests productsRequests = new ProductsRequests();
       // productsRequests.setCompanyId(11);


        Call<List<OutletList>> callEnqueue = userAPICall.getOutletlist("AceNKi4Go8hZ4lLmnS8659VOzY5pgUTyMoEFDU5i");
        callEnqueue.enqueue(new Callback<List<OutletList>>() {
            @Override
            public void onResponse(Call<List<OutletList>> call, Response<List<OutletList>> response) {
                PremiumEngine.myInstance.outletLists = response.body();

                if (PremiumEngine.myInstance.outletLists != null) {
                    Toast.makeText(ContractorDCR.this,
                            "Ready to Process", Toast.LENGTH_SHORT).show();
                    setCustomerSpinner();
                }
                pDialog.dismiss();
            }

            @Override
            public void onFailure(Call<List<OutletList>> call, Throwable t) {
                Toast.makeText(ContractorDCR.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                pDialog.dismiss();
            }
        });
    }

    @OnClick(R.id.dcrSubmit)
    public void submitSale(View view) {
        if(PremiumEngine.isNetworkAvailable(this)){
            pDialog.show();
            dcrRequest =new DcrRequest();
            dcrRequest.setRemarks(enterRemark.getText().toString().trim());
            dcrRequest.setDcrType(dcrType.getSelectedItem().toString());
            dcrRequest.setAddress(comAddress.getText().toString());
            dcrRequest.setCustomerId(PremiumEngine.myInstance.convertStringToInt(comPhones.getText().toString()));

            dcrResponses = new DcrResponses();
            UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);
            Call<DcrResponses> callEnqueue = userAPICall.postDcr("yB4vhewxaggTsQmmlUr41IaC8SWMj8iZdEQNiHnm", dcrRequest);
            callEnqueue.enqueue(new Callback<DcrResponses>() {
                @Override
                public void onResponse(Call<DcrResponses> call, retrofit2.Response<DcrResponses> response) {
                    dcrResponses = response.body();
                    if(dcrResponses != null){
                        Toast.makeText(getApplicationContext(),
                                ""+dcrResponses.getMessage(), Toast.LENGTH_SHORT).show();

                        if(dcrResponses.getMessage().equals("Expense applied successfully")){
                            Intent v =  new Intent(ContractorDCR.this, MainActivity.class);
                            startActivity(v);
                        }

                    }
                    pDialog.dismiss();

                }

                @Override
                public void onFailure(Call<DcrResponses> call, Throwable t) {
                    pDialog.dismiss();
                    Toast.makeText(getApplicationContext(),  t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }else{
            PremiumEngine.myInstance.snackbarNoInternet(this);
        }

    }
}
