package com.ifive.premiumsfa.datas.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Syss4 on 2/9/2018.
 */

public class CompanyListResponse {

    @SerializedName("company_id")
    @Expose
    private String companyId;
    @SerializedName("company_code")
    @Expose
    private String companyCode;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("company_logo_name")
    @Expose
    private String companyLogoName;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompanyLogoName() {
        return companyLogoName;
    }

    public void setCompanyLogoName(String companyLogoName) {
        this.companyLogoName = companyLogoName;
    }

}
