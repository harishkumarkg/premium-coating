package com.ifive.premiumsfa;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ifive.premiumsfa.adapter.BeatListAdapter;
import com.ifive.premiumsfa.adapter.SalesAdapter;
import com.ifive.premiumsfa.adapter.TownListAdapter;
import com.ifive.premiumsfa.adapter.spinner_adapter.CustListAdapter;
import com.ifive.premiumsfa.adapter.spinner_adapter.OutletAdapter;
import com.ifive.premiumsfa.adapter.spinner_adapter.ProductListSpinnerAdapter;
import com.ifive.premiumsfa.adapter.spinner_adapter.StockistListAdapter;
import com.ifive.premiumsfa.classes.RecyclerItemTouchHelper;
import com.ifive.premiumsfa.datas.SessionManager;
import com.ifive.premiumsfa.datas.models.requests.ProductsRequests;
import com.ifive.premiumsfa.datas.models.requests.SalesItemLists;
import com.ifive.premiumsfa.datas.models.requests.SoItemList;
import com.ifive.premiumsfa.datas.models.requests.TownID;
import com.ifive.premiumsfa.datas.models.responses.BeatList;
import com.ifive.premiumsfa.datas.models.responses.CustomerType;
import com.ifive.premiumsfa.datas.models.responses.IDName;
import com.ifive.premiumsfa.datas.models.responses.OutletList;
import com.ifive.premiumsfa.datas.models.responses.ProductList;
import com.ifive.premiumsfa.datas.models.responses.SalesResponse;
import com.ifive.premiumsfa.datas.models.responses.TownList;
import com.ifive.premiumsfa.datas.remote.UserAPICall;
import com.ifive.premiumsfa.engine.PremiumEngine;
import com.ifive.premiumsfa.engine.RetroFitEngine;


import com.ifive.premiumsfa.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Syss4 on 2/2/2018.
 */

public class Movement extends BaseActivity {

    ProgressDialog pDialog;
    @BindView(R.id.town_name)
    TextView sTownName;
    @BindView(R.id.outlet_name)
    TextView sOutletName;
    /*@BindView(R.id.stockist_name)
    TextView sStockistName;*/
    /*@BindView(R.id.beat_name)
    TextView sBeatName;*/
    @BindView(R.id.customer_type)
    TextView sCustomerType;
    @BindView(R.id.items_data_list)
    RecyclerView itemsDataList;
    @BindView(R.id.overtotal_amounts)
    TextView textTotalAmount;
    @BindView(R.id.select_town)
    LinearLayout selectTown;
    @BindView(R.id.select_date)
    LinearLayout selectDate;
    @BindView(R.id.req_date)
    TextView rDate;
    /*@BindView(R.id.select_beat)
    LinearLayout selectBeat;*/
    @BindView(R.id.select_outlet)
    LinearLayout selectOutlet;
    @BindView(R.id.select_ctype)
    LinearLayout selectCust;
    /*@BindView(R.id.select_stockist)
    LinearLayout selectStockist;*/
    @BindView(R.id.add_item)
    ImageButton add;
    @BindView(R.id.blink_text)
    TextView blinkText;
    SessionManager sessionManager;
    SalesAdapter salesAdapter;
    TextView hsnCode, classSpinner, qty, price, gst, total, price_display, uom, aftgst;
    private List<SoItemList> soItemList = new ArrayList<>();
    Spinner productSpinner, proSize;
    private ProductListSpinnerAdapter productListSpinnerAdapter;
    SalesItemLists salesItemLists;
    SalesResponse salesResponse;
    Context myContext;
    double totalAmount;
    Calendar myCalendar;
    DatePickerDialog.OnDateSetListener datePicker;

    int townID = 0, beatID = 0, custID;
    AlertDialog chartAlertDialog;
    AlertDialog.Builder chartDialog;
    private IDName itemData;
    private List<TownList> townLists;
    private List<BeatList> beatLists;
    private List<CustomerType> customerTypes;
    private List<OutletList> outletLists;
    RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale_order);
        pDialog = PremiumEngine.myInstance.getProgDialog(this);
        ButterKnife.bind(this);
        sessionManager = new SessionManager();
        townLists = new ArrayList<>();
        customerTypes = new ArrayList<>();
        getDatasList();
        setItemRecyclerView();

        myCalendar = Calendar.getInstance();
       // pDialog = PremiumEngine.myInstance.getProgDialog(this);
        rDate.setText(PremiumEngine.myInstance.getSimpleCalenderDate(myCalendar));

        datePicker = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                rDate.setText(PremiumEngine.myInstance.getSimpleCalenderDate(myCalendar));
            }
        };


    }
    @Override
    public void onResume() {
        super.onResume();
        calculateTotalAmount();
    }

    private void setItemRecyclerView() {
        salesAdapter = new SalesAdapter(this, PremiumEngine.myInstance.soItemLists, this);
        itemsDataList.setAdapter(salesAdapter);
        mLayoutManager = new LinearLayoutManager(this);
        itemsDataList.setLayoutManager(mLayoutManager);
        itemsDataList.setItemAnimator(new DefaultItemAnimator());
        itemsDataList.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, null);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(itemsDataList);
    }


    private String addNumbers() {
        int number1;
        int number2;
        if (price.getText().toString() != "" && price.getText().length() > 0) {
            number1 = Integer.parseInt(price.getText().toString());
        } else {
            number1 = 0;
        }
        if (qty.getText().toString() != "" && qty.getText().length() > 0) {
            number2 = Integer.parseInt(qty.getText().toString());
        } else {
            number2 = 0;
        }

        return Integer.toString(number1 * number2);
    }

    private String aftergst() {
        int number1;
        int number2;
        int number3;

        number3 = 100;

        if (price_display.getText().toString() != "" && price_display.getText().length() > 0) {
            number1 = Integer.parseInt(price_display.getText().toString());

        } else {
            number1 = 0;
        }
        if (gst.getText().toString() != "" && gst.getText().length() > 0) {
            number2 = Integer.parseInt(gst.getText().toString());
        } else {
            number2 = 0;
        }
        //return Integer.toString(number1 * number2 / number3 + number1);
        return Integer.toString(number1 * number2 / number3);
    }

    private String overallTotal() {
        int number1;
        int number2;
        int number3;

        number3 = 100;

        if (price_display.getText().toString() != "" && price_display.getText().length() > 0) {
            number1 = Integer.parseInt(price_display.getText().toString());

        } else {
            number1 = 0;
        }
        if (gst.getText().toString() != "" && gst.getText().length() > 0) {
            number2 = Integer.parseInt(gst.getText().toString());
        } else {
            number2 = 0;
        }
        //return Integer.toString(number1 * number2 / number3 + number1);
        return Integer.toString(number1 * number2 / number3 + number1);
    }

    @OnClick(R.id.add_item)
    public void addItemClick(View view) {
        View addItemView = LayoutInflater.from(this)
                .inflate(R.layout.add_order_layout, null);
        android.app.AlertDialog.Builder chartDialog = new android.app.AlertDialog.Builder(this);
        chartDialog.setView(addItemView);
        final android.app.AlertDialog chartAlertDialog = chartDialog.show();
        productSpinner = addItemView.findViewById(R.id.product_spinner);
        proSize = addItemView.findViewById(R.id.pro_size);
        classSpinner = addItemView.findViewById(R.id.pro_prices);
        qty = addItemView.findViewById(R.id.pro_qtys);
        hsnCode = addItemView.findViewById(R.id.hsn_code);
        price = addItemView.findViewById(R.id.pro_prices);
        gst = addItemView.findViewById(R.id.pro_gsts);
        price_display = addItemView.findViewById(R.id.price_displays);
        uom = addItemView.findViewById(R.id.uom_code);
        total = addItemView.findViewById(R.id.pro_totals);
        aftgst = addItemView.findViewById(R.id.after_gst);
        setProductSpinner();


        // setClassSpinner();

        Button dismissItem = addItemView.findViewById(R.id.dismiss_item);
        Button addItem = addItemView.findViewById(R.id.add_item);
        dismissItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chartAlertDialog.dismiss();
            }
        });

        qty.addTextChangedListener(new TextWatcher() {

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                price_display.setText(addNumbers());
                total.setText(overallTotal());
//                totalAmounts.setText(overallTotal());
            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });

        gst.addTextChangedListener(new TextWatcher() {

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

                total.setText(overallTotal());
                aftgst.setText(aftergst());
            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });

        addItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chartAlertDialog.dismiss();
                addItemToRecycler();
            }

        });

    }

    @OnClick(R.id.submit_sale)
    public void submitSale(View view) {
        if(PremiumEngine.isNetworkAvailable(this)){
            if(PremiumEngine.myInstance.soItemLists.size()!=0){
                pDialog.show();
                salesItemLists =new SalesItemLists();
                salesItemLists.setTown(sTownName.getText().toString());
               // salesItemLists.setStockist(sStockistName.getText().toString());
                //salesItemLists.setBeat(sBeatName.getText().toString());
                salesItemLists.setOutlet(sOutletName.getText().toString());
                salesItemLists.setCustomerType(sCustomerType.getText().toString());
                salesItemLists.setItemsLists(PremiumEngine.myInstance.soItemLists);
                salesItemLists.setTotalAmount(textTotalAmount.getText().toString());
                salesItemLists.setUserId(sessionManager.getUserData(this).getUserId().toString());
                salesItemLists.setOrgId(sessionManager.getUserData(this).getSsDefaultorgId());
                salesItemLists.setLocationId(sessionManager.getUserData(this).getSsDefaultlocId());
                salesItemLists.setCustomerId(sessionManager.getOutLet(this).getId());
                salesResponse = new SalesResponse();
                UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);
                Call<SalesResponse> callEnqueue = userAPICall.postSale("yB4vhewxaggTsQmmlUr41IaC8SWMj8iZdEQNiHnm" ,salesItemLists);
                callEnqueue.enqueue(new Callback<SalesResponse>() {
                    @Override
                    public void onResponse(Call<SalesResponse> call, retrofit2.Response<SalesResponse> response) {
                        salesResponse = response.body();
                        if(salesResponse != null){
                            Toast.makeText(getApplicationContext(),
                                    ""+salesResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            if(salesResponse.getMessage().equals("Po Approved successfully")){
                                Intent b = new Intent(Movement.this, OrderList.class);
                                startActivity(b);
                                resetSale();
                            }
                        }
                        pDialog.dismiss();
                    }
                    @Override
                    public void onFailure(Call<SalesResponse> call, Throwable t) {
                        pDialog.dismiss();
                        Toast.makeText(myContext,  t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }else{
                Toast.makeText(this, "Items not found", Toast.LENGTH_SHORT).show();
            }
        }else{
            PremiumEngine.myInstance.snackbarNoInternet(this);
        }
    }

    @OnClick(R.id.reset_sale)
    public void resetSale() {
        PremiumEngine.myInstance.soItemLists.clear();
        salesAdapter.notifyDataSetChanged();
        calculateTotalAmount();
    }

    public void addItemToRecycler() {
        SoItemList soItemList = new SoItemList();
        soItemList = new SoItemList();
        soItemList.setProductName(PremiumEngine.myInstance.itemSelected.getConcatSegment());
        soItemList.setHsnCode(hsnCode.getText().toString());
        soItemList.setUnitPrice(classSpinner.getText().toString());
        soItemList.setQty(qty.getText().toString());
        soItemList.setPrice(price.getText().toString());
        soItemList.setGst(gst.getText().toString());
        soItemList.setTotal(PremiumEngine.myInstance.convertStringToDouble(total.getText().toString()));
        soItemList.setGstCalculation(aftgst.getText().toString());
        soItemList.setUomCodeId(uom.getText().toString());
        soItemList.setProSize(proSize.getSelectedItem().toString());
        PremiumEngine.myInstance.soItemLists.add(soItemList);
        calculateTotalAmount();
    }


    public void calculateTotalAmount() {
        totalAmount = 0;
        for (SoItemList itemList :PremiumEngine.myInstance.soItemLists) {
            double productPrice =itemList.getTotal();
            totalAmount += productPrice;
        }
        textTotalAmount.setText(totalAmount+"");
        if (PremiumEngine.myInstance.soItemLists.size() == 0) {
            itemsDataList.setVisibility(View.GONE);
            blinkText.setVisibility(View.VISIBLE);
            blinkText.startAnimation(PremiumEngine.myInstance.getBlinkAnimation());
        } else {
            itemsDataList.setVisibility(View.VISIBLE);
            blinkText.clearAnimation();
            blinkText.setVisibility(View.GONE);
        }
        salesAdapter.notifyDataSetChanged();
    }

    private void setProductSpinner() {
        productListSpinnerAdapter = new ProductListSpinnerAdapter(this,
                android.R.layout.simple_spinner_item,
                PremiumEngine.productList);
        productSpinner.setAdapter(productListSpinnerAdapter);
        productSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                // Here you get the current item (a User object) that is selected by its position
                PremiumEngine.myInstance.itemSelected = productListSpinnerAdapter.getItem(position);
                hsnCode.setText(productListSpinnerAdapter.getItem(position).getHsnCode());
                classSpinner.setText(productListSpinnerAdapter.getItem(position).getPrice());
                uom.setText(productListSpinnerAdapter.getItem(position).getUomCodeId());
                gst.setText(productListSpinnerAdapter.getItem(position).getDisplayName());
//                Log.w("gst_value",productListSpinnerAdapter.getItem(position).getDisplayName());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });
    }

    @OnClick(R.id.select_town)
    public void selectTown() {
        if (PremiumEngine.isNetworkAvailable(this)) {
            pDialog.show();
            UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);

            Call<List<TownList>> callEnqueue = userAPICall.town(null);
            callEnqueue.enqueue(new Callback<List<TownList>>() {
                @Override
                public void onResponse(Call<List<TownList>> call, Response<List<TownList>> response) {
                    if (response.body() != null) {
                        townLists = response.body();
                        getTownList();
                    }
                    pDialog.dismiss();
                }

                @Override
                public void onFailure(Call<List<TownList>> call, Throwable t) {
                    Toast.makeText(Movement.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    pDialog.dismiss();
                }
            });
        } else {
            PremiumEngine.myInstance.snackbarNoInternet(this);
        }
    }

    @OnClick(R.id.select_date)
    public void selectReqDate() {
        new DatePickerDialog(this, datePicker, myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    public void getTownList() {
        View addItemView = LayoutInflater.from(this)
                .inflate(R.layout.autosearch_recycler, null);
        chartDialog = new AlertDialog.Builder(this);
        chartDialog.setView(addItemView);
        chartAlertDialog = chartDialog.show();
        chartAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RecyclerView townsDataList = addItemView.findViewById(R.id.items_data_list);
        TextView textTitle = addItemView.findViewById(R.id.text_title);
        final EditText search_type = addItemView.findViewById(R.id.search_type);
        textTitle.setText("Town List");
        final TownListAdapter itemShowAdapter = new TownListAdapter(this, townLists, this, townID);
        townsDataList.setAdapter(itemShowAdapter);
        mLayoutManager = new LinearLayoutManager(this);
        townsDataList.setLayoutManager(mLayoutManager);
        townsDataList.setItemAnimator(new DefaultItemAnimator());
        search_type.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = search_type.getText().toString().toLowerCase(Locale.getDefault());
                itemShowAdapter.filter(text);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        });
    }

    @OnClick(R.id.select_outlet)
    public void selectOutlet() {
        if (PremiumEngine.isNetworkAvailable(this)) {
            pDialog.show();
            UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);

            Call<List<OutletList>> callEnqueue = userAPICall.getOutletlist(null);
            callEnqueue.enqueue(new Callback<List<OutletList>>() {
                @Override
                public void onResponse(Call<List<OutletList>> call, Response<List<OutletList>> response) {
                    if (response.body() != null) {
                        outletLists = response.body();
                        getOutlet();
                    }
                    pDialog.dismiss();
                }

                @Override
                public void onFailure(Call<List<OutletList>> call, Throwable t) {
                    Toast.makeText(Movement.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    pDialog.dismiss();
                }
            });
        } else {
            PremiumEngine.myInstance.snackbarNoInternet(this);
        }
    }

    public void getOutlet() {
        View addItemView = LayoutInflater.from(this)
                .inflate(R.layout.autosearch_recycler, null);
        chartDialog = new AlertDialog.Builder(this);
        chartDialog.setView(addItemView);
        chartAlertDialog = chartDialog.show();
        chartAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RecyclerView townsDataList = addItemView.findViewById(R.id.items_data_list);
        TextView textTitle = addItemView.findViewById(R.id.text_title);
        final EditText search_type = addItemView.findViewById(R.id.search_type);
        textTitle.setText("Outlet List");
        final OutletAdapter itemShowAdapter = new OutletAdapter(this, outletLists, this, townID);
        townsDataList.setAdapter(itemShowAdapter);
        mLayoutManager = new LinearLayoutManager(this);
        townsDataList.setLayoutManager(mLayoutManager);
        townsDataList.setItemAnimator(new DefaultItemAnimator());
        search_type.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = search_type.getText().toString().toLowerCase(Locale.getDefault());
                itemShowAdapter.filter(text);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        });
    }

    @OnClick(R.id.select_ctype)
    public void selectCust() {
        if (PremiumEngine.isNetworkAvailable(this)) {
            pDialog.show();
            UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);

            Call<List<CustomerType>> callEnqueue = userAPICall.customer(null);
            callEnqueue.enqueue(new Callback<List<CustomerType>>() {
                @Override
                public void onResponse(Call<List<CustomerType>> call, Response<List<CustomerType>> response) {
                    if (response.body() != null) {
                        customerTypes = response.body();
                        getCustList();
                    }
                    pDialog.dismiss();
                }

                @Override
                public void onFailure(Call<List<CustomerType>> call, Throwable t) {
                    Toast.makeText(Movement.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    pDialog.dismiss();
                }
            });
        } else {
            PremiumEngine.myInstance.snackbarNoInternet(this);
        }
    }

    public void getCustList() {
        View addItemView = LayoutInflater.from(this)
                .inflate(R.layout.autosearch_recycler, null);
        chartDialog = new AlertDialog.Builder(this);
        chartDialog.setView(addItemView);
        chartAlertDialog = chartDialog.show();
        chartAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RecyclerView townsDataList = addItemView.findViewById(R.id.items_data_list);
        TextView textTitle = addItemView.findViewById(R.id.text_title);
        final EditText search_type = addItemView.findViewById(R.id.search_type);
        textTitle.setText("Cust List");
        final CustListAdapter itemShowAdapter = new CustListAdapter(this, customerTypes, this, townID);
        townsDataList.setAdapter(itemShowAdapter);
        mLayoutManager = new LinearLayoutManager(this);
        townsDataList.setLayoutManager(mLayoutManager);
        townsDataList.setItemAnimator(new DefaultItemAnimator());
        search_type.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = search_type.getText().toString().toLowerCase(Locale.getDefault());
                itemShowAdapter.filter(text);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        });
    }

   /* @OnClick(R.id.select_beat)
    public void selectBeat() {
        if (PremiumEngine.isNetworkAvailable(this)) {

            pDialog.show();
            UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);
            TownID townID = new TownID();
            townID.setTownID(1);
            Call<List<BeatList>> callEnqueue = userAPICall.beat(null,townID);
            callEnqueue.enqueue(new Callback<List<BeatList>>() {

                @Override
                public void onResponse(Call<List<BeatList>> call, Response<List<BeatList>> response) {
                    if (response.body() != null) {
                        beatLists = response.body();
                        getBeatList();
                    }
                    pDialog.dismiss();
                }

                @Override
                public void onFailure(Call<List<BeatList>> call, Throwable t) {
                    Toast.makeText(Movement.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    pDialog.dismiss();
                }
            });

        } else {
            PremiumEngine.myInstance.snackbarNoInternet(this);
        }
    }*/

    public void getBeatList() {
        View addItemView = LayoutInflater.from(this)
                .inflate(R.layout.autosearch_recycler, null);
        chartDialog = new AlertDialog.Builder(this);
        chartDialog.setView(addItemView);
        chartAlertDialog = chartDialog.show();
        RecyclerView townsDataList = addItemView.findViewById(R.id.items_data_list);
        TextView textTitle = addItemView.findViewById(R.id.text_title);
        final EditText search_type = addItemView.findViewById(R.id.search_type);
        textTitle.setText("Beat List");
        final BeatListAdapter itemShowAdapter = new BeatListAdapter(this, beatLists, this, beatID);
        townsDataList.setAdapter(itemShowAdapter);
        mLayoutManager = new LinearLayoutManager(this);
        townsDataList.setLayoutManager(mLayoutManager);
        townsDataList.setItemAnimator(new DefaultItemAnimator());
        search_type.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = search_type.getText().toString().toLowerCase(Locale.getDefault());
                itemShowAdapter.filter(text);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        });
    }

    /*@OnClick(R.id.select_stockist)
    public void seleStockist() {
        if (PremiumEngine.isNetworkAvailable(this)) {
            pDialog.show();
            UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);

            Call<List<OutletList>> callEnqueue = userAPICall.getOutletlist(null);
            callEnqueue.enqueue(new Callback<List<OutletList>>() {
                @Override
                public void onResponse(Call<List<OutletList>> call, Response<List<OutletList>> response) {
                    if (response.body() != null) {
                        outletLists = response.body();
                        getStockist();
                    }
                    pDialog.dismiss();
                }

                @Override
                public void onFailure(Call<List<OutletList>> call, Throwable t) {
                    Toast.makeText(Movement.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    pDialog.dismiss();
                }
            });
        } else {
            PremiumEngine.myInstance.snackbarNoInternet(this);
        }
    }

    public void getStockist() {
        View addItemView = LayoutInflater.from(this)
                .inflate(R.layout.autosearch_recycler, null);
        chartDialog = new AlertDialog.Builder(this);
        chartDialog.setView(addItemView);
        chartAlertDialog = chartDialog.show();
        chartAlertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        RecyclerView townsDataList = addItemView.findViewById(R.id.items_data_list);
        TextView textTitle = addItemView.findViewById(R.id.text_title);
        final EditText search_type = addItemView.findViewById(R.id.search_type);
        textTitle.setText("Stockist List");
        final StockistListAdapter itemShowAdapter = new StockistListAdapter(this, outletLists, this, townID);
        townsDataList.setAdapter(itemShowAdapter);
        mLayoutManager = new LinearLayoutManager(this);
        townsDataList.setLayoutManager(mLayoutManager);
        townsDataList.setItemAnimator(new DefaultItemAnimator());
        search_type.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = search_type.getText().toString().toLowerCase(Locale.getDefault());
                itemShowAdapter.filter(text);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        });
    }*/


    public void dismissAlert() {
        if (chartAlertDialog != null && chartAlertDialog.isShowing()) {
            chartAlertDialog.dismiss();
        }
    }

    private boolean setTitles() {
        boolean status = true;
        itemData = sessionManager.getTownData(this);
        if (itemData.getId() != 0 && itemData.getName() != null) {
            sTownName.setText(itemData.getName() + "");
            townID = itemData.getId();
        } else {
            status = false;
            sTownName.setText("--Select--");
        }

        itemData = sessionManager.getBeatId(this);
        if (itemData.getId() != 0 && itemData.getName() != null) {
           // sBeatName.setText(itemData.getName() + "");
            townID = itemData.getId();
        } else {
            status = false;
           // sBeatName.setText("--Select--");
        }

        itemData = sessionManager.getCustType(this);
        if(itemData.getId()!=0 && itemData.getName()!=null){
            sCustomerType.setText(itemData.getName()+"");
            custID = itemData.getId();
        }else{
            status =false;
            sCustomerType.setText("--Select--");
        }

        itemData = sessionManager.getOutLet(this);
        if(itemData.getId()!=0 && itemData.getName()!=null){
            sOutletName.setText(itemData.getName()+"");
            custID = itemData.getId();
        }else{
            status =false;
            sOutletName.setText("--Select--");
        }

        itemData = sessionManager.getStockist(this);
        if(itemData.getId()!=0 && itemData.getName()!=null){
            //sStockistName.setText(itemData.getName()+"");
            custID = itemData.getId();
        }else{
            status =false;
           // sStockistName.setText("--Select--");
        }

        return status;
    }

    public void setSaleTownPreference(int id, String name) {
        dismissAlert();
        sessionManager = new SessionManager();
        sessionManager.setSaleTown(this, id, name);
        townID = id;
        setTitles();
        /*selectTown();*/
    }

    public void setSaleOutletPreference(int id, String name) {
        dismissAlert();
        sessionManager = new SessionManager();
        sessionManager.setOutLet(this, id, name);
        townID = id;
        setTitles();
        //selectTown();
    }

    public void setBeatPreference(int id, String name) {
        dismissAlert();
        sessionManager = new SessionManager();
        sessionManager.setBeatID(this, id, name);
        townID = id;
        setTitles();
        //selectTown();
    }

    public void setStockistPreference(int id, String name) {
        dismissAlert();
        sessionManager = new SessionManager();
        sessionManager.setStockist(this, id, name);
        townID = id;
        setTitles();
        /*selectTown();*/
    }

    public void setSaleCustPreference(int id, String name) {
        dismissAlert();
        sessionManager = new SessionManager();
        sessionManager.setCustType(this, id, name);
        townID = id;
        setTitles();
        /*selectTown();*/
    }

    public void setSaleBeatPreference(int id, String name) {
        dismissAlert();
        sessionManager = new SessionManager();
        sessionManager.setBeatID(this, id, name);
        beatID = id;
        setTitles();
        //selectOutlet();
    }

    public void getDatasList() {
        pDialog.show();
        PremiumEngine.myInstance.productList = new ArrayList<>();
        UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);
        ProductsRequests productsRequests = new ProductsRequests();
        productsRequests.setCompanyId(11);


        Call<List<ProductList>> callEnqueue = userAPICall.product(/*sessionManager.getToken(this)*/"AceNKi4Go8hZ4lLmnS8659VOzY5pgUTyMoEFDU5i", productsRequests);
        callEnqueue.enqueue(new Callback<List<ProductList>>() {
            @Override
            public void onResponse(Call<List<ProductList>> call, Response<List<ProductList>> response) {
                PremiumEngine.myInstance.productList = response.body();

                if (PremiumEngine.myInstance.productList != null) {
                    Toast.makeText(Movement.this,
                            "Ready to Process", Toast.LENGTH_SHORT).show();
                }
                pDialog.dismiss();
            }

            @Override
            public void onFailure(Call<List<ProductList>> call, Throwable t) {
                Toast.makeText(Movement.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                pDialog.dismiss();
            }
        });
    }

}
