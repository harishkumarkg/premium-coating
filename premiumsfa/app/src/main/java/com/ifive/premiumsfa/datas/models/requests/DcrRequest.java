package com.ifive.premiumsfa.datas.models.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Syss4 on 2/19/2018.
 */

public class DcrRequest {

    @SerializedName("dcr_type")
    @Expose
    private String dcrType;
    @SerializedName("customer_id")
    @Expose
    private Integer customerId;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("remarks")
    @Expose
    private String remarks;

    public String getDcrType() {
        return dcrType;
    }

    public void setDcrType(String dcrType) {
        this.dcrType = dcrType;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

}
