package com.ifive.premiumsfa;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ifive.premiumsfa.adapter.Company_Adapter;
import com.ifive.premiumsfa.adapter.SalesOrderAdapter;
import com.ifive.premiumsfa.datas.SessionManager;
import com.ifive.premiumsfa.datas.models.responses.CompanyListResponse;
import com.ifive.premiumsfa.datas.models.responses.SalesList;
import com.ifive.premiumsfa.datas.remote.UserAPICall;
import com.ifive.premiumsfa.engine.PremiumEngine;
import com.ifive.premiumsfa.engine.RetroFitEngine;
import com.ifive.premiumsfa.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Syss4 on 2/16/2018.
 */

public class OrderList extends BaseActivity {

    private ProgressDialog pDialog;
    SessionManager sessionManager;
    SalesOrderAdapter salesOrderAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    Context myContext;

    List<SalesList> salesLists;
    @BindView(R.id.gatepass_swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.sales_message)
    TextView messageDisplay;
    @BindView(R.id.orderList)
    RecyclerView gatepass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.orderlist);
        pDialog = PremiumEngine.myInstance.getProgDialog(this);
        ButterKnife.bind(this);
        sessionManager = new SessionManager();
        getGatepass();
    }

    public void getGatepass() {
        pDialog.show();
        salesLists = new ArrayList<>();
        UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);
        Call<List<SalesList>> callEnqueue = userAPICall.getSale(sessionManager.getToken(this));
        callEnqueue.enqueue(new Callback<List<SalesList>>() {
            @Override
            public void onResponse(Call<List<SalesList>> call, Response<List<SalesList>> response) {
                salesLists = response.body();
                if(salesLists != null){
                    setItemRecyclerView();
                }
                pDialog.dismiss();

            }

            @Override
            public void onFailure(Call<List<SalesList>> call, Throwable t) {
                pDialog.dismiss();
                //  Toast.makeText(myContext,  t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setItemRecyclerView() {
        if(salesLists.size() == 0){
            gatepass.setVisibility(View.GONE);
            messageDisplay.setVisibility(View.VISIBLE);
        }else{
            messageDisplay.setVisibility(View.GONE);
            gatepass.setVisibility(View.VISIBLE);
            salesOrderAdapter = new SalesOrderAdapter(this, salesLists,this);
            gatepass.setAdapter(salesOrderAdapter);
            mLayoutManager = new LinearLayoutManager(this);
            gatepass.setLayoutManager(mLayoutManager);
            gatepass.setItemAnimator(new DefaultItemAnimator());
            gatepass.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        }
    }


    public void onRefresh() {
        getGatepass();
    }
}
