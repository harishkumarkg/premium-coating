package com.ifive.premiumsfa.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.ifive.premiumsfa.R;
import com.ifive.premiumsfa.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by iftpc011 on 16/3/18.
 */

public class LeaveEntry extends BaseActivity {

    @BindView(R.id.leaveEntry)
    Button leaveReq;
    @BindView(R.id.leaveList)
    RecyclerView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.leave_entry);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.leaveEntry)
    public void clickRemarks(View view) {
        Intent n = new Intent(this, EnterLeave.class);
        startActivity(n);
    }
}
