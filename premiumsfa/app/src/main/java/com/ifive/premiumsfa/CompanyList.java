package com.ifive.premiumsfa;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ifive.premiumsfa.adapter.Company_Adapter;
import com.ifive.premiumsfa.datas.SessionManager;
import com.ifive.premiumsfa.datas.models.responses.CompanyListResponse;
import com.ifive.premiumsfa.datas.remote.UserAPICall;
import com.ifive.premiumsfa.engine.PremiumEngine;
import com.ifive.premiumsfa.engine.RetroFitEngine;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Syss4 on 2/8/2018.
 */

public class CompanyList extends AppCompatActivity {

    @BindView(R.id.compList)
    RecyclerView collictionList;
    @BindView(R.id.company_message)
    TextView messageDisplay;
    private ProgressDialog pDialog;
    SessionManager sessionManager;
    Context myContext;
    Company_Adapter company_adapter;
    RecyclerView.LayoutManager mLayoutManager;

    List<CompanyListResponse> company_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.company_list);
        ButterKnife.bind(this);
        pDialog = PremiumEngine.myInstance.getProgDialog(this);
        sessionManager = new SessionManager();
        getGatepass();
    }

    public void getGatepass() {
        pDialog.show();
        company_list = new ArrayList<>();
        UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);
        Call<List<CompanyListResponse>> callEnqueue = userAPICall.getCompanylist(sessionManager.getToken(this));
        callEnqueue.enqueue(new Callback<List<CompanyListResponse>>() {
            @Override
            public void onResponse(Call<List<CompanyListResponse>> call, Response<List<CompanyListResponse>> response) {
                company_list = response.body();
                if(company_list != null){
                    setItemRecyclerView();
                }
                pDialog.dismiss();

            }

            @Override
            public void onFailure(Call<List<CompanyListResponse>> call, Throwable t) {
                pDialog.dismiss();
              //  Toast.makeText(myContext,  t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setItemRecyclerView() {

        if(company_list.size() == 0){
            collictionList.setVisibility(View.GONE);
            messageDisplay.setVisibility(View.VISIBLE);
        }else{
            messageDisplay.setVisibility(View.GONE);
            collictionList.setVisibility(View.VISIBLE);
            company_adapter = new Company_Adapter(this, company_list,this);
            collictionList.setAdapter(company_adapter);
            mLayoutManager = new LinearLayoutManager(this);
            collictionList.setLayoutManager(mLayoutManager);
            collictionList.setItemAnimator(new DefaultItemAnimator());
            collictionList.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        }
    }
}
