package com.ifive.premiumsfa.datas.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Syss4 on 2/15/2018.
 */

public class SalesResponse {

    @SerializedName("collection_id")
    @Expose
    private Integer collectionId;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getCollectionId() {
        return collectionId;
    }

    public void setCollectionId(Integer collectionId) {
        this.collectionId = collectionId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
