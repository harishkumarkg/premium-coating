package com.ifive.premiumsfa.adapter.spinner_adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ifive.premiumsfa.Movement;
import com.ifive.premiumsfa.R;
import com.ifive.premiumsfa.datas.SessionManager;
import com.ifive.premiumsfa.datas.models.responses.OutletList;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Syss4 on 2/15/2018.
 */

public class StockistListAdapter extends RecyclerView.Adapter<StockistListAdapter.MyViewHolder> {
    private Context context;
    private List<OutletList> cartList;
    private ArrayList<OutletList> townList;
    Movement movement;
    SessionManager sessionManager;
    Integer townID;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout viewForeground;
        public TextView townName, serialNumber;
        public MyViewHolder(View view) {
            super(view);
            townName = view.findViewById(R.id.cust_names);
            serialNumber = view.findViewById(R.id.serial_numbers);
            viewForeground = view.findViewById(R.id.view_foregrounds);
        }
    }

    public StockistListAdapter(Context context, List<OutletList> cartList, Movement movement, int townID) {
        this.context = context;
        this.cartList = cartList;
        this.movement = movement;
        this.townID = townID;
        this.townList = new ArrayList<OutletList>();
        this.townList.addAll(cartList);
    }

    @Override
    public StockistListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cust_list, parent, false);

        return new StockistListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(StockistListAdapter.MyViewHolder holder, final int position) {
        final OutletList item = cartList.get(position);
        holder.townName.setText(item.getCustomerSiteName());
        holder.serialNumber.setText(position+1+"");
        holder.viewForeground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                movement.setStockistPreference(item.getCustomerSiteId(),item.getCustomerSiteName());
            }
        });
        if((townID != 0) && (townID.equals(item.getCustomerSiteId()))){
            holder.viewForeground.setBackground(context.getResources().getDrawable(R.drawable.green_bg));
            holder.townName.setTextColor(context.getResources().getColor(R.color.white));
        }else{
            holder.viewForeground.setBackground(context.getResources().getDrawable(R.drawable.gradient_bg2));
            holder.townName.setTextColor(context.getResources().getColor(R.color.black_low));
        }
    }

    @Override
    public int getItemCount() {
        return cartList.size();
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        cartList.clear();
        if (charText.length() == 0) {
            cartList.addAll(townList);
        }else{
            for (OutletList wp : townList){
                if (wp.getCustomerSiteName().toLowerCase(Locale.getDefault()).contains(charText)){
                    cartList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}
