package com.ifive.premiumsfa.datas.remote;

import com.ifive.premiumsfa.datas.models.requests.AttendanceListRequest;
import com.ifive.premiumsfa.datas.models.requests.AttendanceRequest;
import com.ifive.premiumsfa.datas.models.requests.DcrRequest;
import com.ifive.premiumsfa.datas.models.requests.ExpensesRequest;
import com.ifive.premiumsfa.datas.models.requests.LoginRequest;
import com.ifive.premiumsfa.datas.models.requests.ProductsRequests;
import com.ifive.premiumsfa.datas.models.requests.SalesItemLists;
import com.ifive.premiumsfa.datas.models.requests.TownID;
import com.ifive.premiumsfa.datas.models.responses.AttendListModel;
import com.ifive.premiumsfa.datas.models.responses.AttendanceResponse;
import com.ifive.premiumsfa.datas.models.responses.BeatList;
import com.ifive.premiumsfa.datas.models.responses.CompanyListResponse;
import com.ifive.premiumsfa.datas.models.responses.CustomerType;
import com.ifive.premiumsfa.datas.models.responses.DcrResponses;
import com.ifive.premiumsfa.datas.models.responses.ExpensesResponses;
import com.ifive.premiumsfa.datas.models.responses.LoginResponse;
import com.ifive.premiumsfa.datas.models.responses.OutletList;
import com.ifive.premiumsfa.datas.models.responses.ProductList;
import com.ifive.premiumsfa.datas.models.responses.SalesList;
import com.ifive.premiumsfa.datas.models.responses.SalesResponse;
import com.ifive.premiumsfa.datas.models.responses.TownList;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface UserAPICall {

   /* @POST("premium/public/mobileapputility/validateuser")
    Call<LoginResponse> login(@Body LoginRequest loginRequest);

    @GET("premium/public/mobileapputility/companydetails")
    Call<List<CompanyListResponse>> getCompanylist(@Header("token") String token);

    @POST("premium/public/mobileapputility/listtown")
    Call<List<TownList>> town(@Header("token") String token);

    @POST("premium/public/mobileapputility/listbeats")
    Call<List<BeatList>> beat(@Header("token") String token);

    @POST("premium/public/mobileapputility/listcustomertypes")
    Call<List<CustomerType>> customer(@Header("token") String token);

    @POST("premium/public/mobileapputility/listproduct")
    Call<List<ProductList>> product(@Header("token") String token, @Body ProductsRequests productsRequests);

    @GET("premium/public/mobileapputility/listoutlets")
    Call<List<OutletList>> getOutletlist(@Header("token") String token);

    @POST("premium/public/mobileapputility/loadsalesorder")
    Call<SalesResponse> postSale(@Header("token") String token, @Body SalesItemLists salesItemLists);

    @GET("premium/public/mobileapputility/listsalesorder")
    Call<List<SalesList>> getSale(@Header("token") String token);

    @POST("premium/public/mobileapputility/loadexpense")
    Call<ExpensesResponses> postExpen(@Header("token") String token, @Body ExpensesRequest tech_issue);

    @POST("premium/public/mobileapputility/loaddcr")
    Call<DcrResponses> postDcr(@Header("token") String token, @Body DcrRequest dcrRequest);*/


    @POST("premiumcapp/client/premiumcuatprod/public/mobileapputility/validateuser")
    Call<LoginResponse> login(@Body LoginRequest loginRequest);

    @GET("premiumcapp/client/premiumcuatprod/public/mobileapputility/companydetails")
    Call<List<CompanyListResponse>> getCompanylist(@Header("token") String token);

    @POST("premiumcapp/client/premiumcuatprod/public/mobileapputility/listtown")
    Call<List<TownList>> town(@Header("token") String token);

    @POST("premiumcapp/client/premiumcuatprod/public/mobileapputility/listbeats")
    Call<List<BeatList>> beat(@Header("token") String token, @Body TownID townID);

    @POST("premiumcapp/client/premiumcuatprod/public/mobileapputility/listcustomertypes")
    Call<List<CustomerType>> customer(@Header("token") String token);

    @POST("premiumcapp/client/premiumcuatprod/public/mobileapputility/listproduct")
    Call<List<ProductList>> product(@Header("token") String token, @Body ProductsRequests productsRequests);

    @GET("premiumcapp/client/premiumcuatprod/public/mobileapputility/listoutlets")
    Call<List<OutletList>> getOutletlist(@Header("token") String token);

    @POST("premiumcapp/client/premiumcuatprod/public/mobileapputility/loadsalesorder")
    Call<SalesResponse> postSale(@Header("token") String token, @Body SalesItemLists salesItemLists);

    @GET("premiumcapp/client/premiumcuatprod/public/mobileapputility/listsalesorder")
    Call<List<SalesList>> getSale(@Header("token") String token);

    @POST("premiumcapp/client/premiumcuatprod/public/mobileapputility/loadexpense")
    Call<ExpensesResponses> postExpen(@Header("token") String token, @Body ExpensesRequest tech_issue);

    @POST("premiumcapp/client/premiumcuatprod/public/mobileapputility/loaddcr")
    Call<DcrResponses> postDcr(@Header("token") String token, @Body DcrRequest dcrRequest);

    @POST("premiumcapp/client/premiumcuatprod/public/mobileapputility/attendanceentry")
    Call<AttendanceResponse> postAtten(@Header("token") String token, @Body AttendanceRequest attendanceRequest);

    @POST("premiumcapp/client/premiumcuatprod/public/mobileapputility/attendancelist")
    Call<AttendListModel> listAtten(@Header("token") String token, @Body AttendanceListRequest attendanceListRequest);

}
