package com.ifive.premiumsfa.datas.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Syss4 on 2/14/2018.
 */

public class CustomerType {

    @SerializedName("customer_type_id")
    @Expose
    private Integer customerTypeId;
    @SerializedName("customer_type")
    @Expose
    private String customerType;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("organization_id")
    @Expose
    private String organizationId;
    @SerializedName("created_by")
    @Expose
    private Object createdBy;
    @SerializedName("created_date")
    @Expose
    private Object createdDate;
    @SerializedName("last_updated_by")
    @Expose
    private Object lastUpdatedBy;
    @SerializedName("last_updated_date")
    @Expose
    private Object lastUpdatedDate;

    public Integer getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(Integer customerTypeId) {
        this.customerTypeId = customerTypeId;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public Object getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Object createdBy) {
        this.createdBy = createdBy;
    }

    public Object getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Object createdDate) {
        this.createdDate = createdDate;
    }

    public Object getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(Object lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public Object getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(Object lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }


}
