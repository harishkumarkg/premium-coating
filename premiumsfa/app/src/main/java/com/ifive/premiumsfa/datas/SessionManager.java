package com.ifive.premiumsfa.datas;

import android.content.Context;
import android.content.SharedPreferences;

import com.ifive.premiumsfa.datas.models.responses.IDName;
import com.ifive.premiumsfa.datas.models.responses.LoginResponse;
import com.ifive.premiumsfa.engine.PremiumEngine;


public class SessionManager {

    public void setPreferences(Context context, LoginResponse loginResponse){
        SharedPreferences.Editor editor = context.getSharedPreferences("Premium", Context.MODE_PRIVATE).edit();
        editor.putString("Token", loginResponse.getRememberToken());
        editor.putString("user_id",loginResponse.getUserId()+"");
        editor.putString("Username",loginResponse.getUsername());
        editor.putString("Role",loginResponse.getEmail()+"");
        editor.putString("CompanyId", loginResponse.getCompanyId());
        editor.putString("LocationId", loginResponse.getSsDefaultlocId());
        editor.putString("OrgId", loginResponse.getSsDefaultorgId());
        editor.putString("first_name", loginResponse.getFirstName());
        editor.putString("last_name", loginResponse.getLastName());
        editor.putString("emp_id", loginResponse.getEmpId()+"");
        editor.commit();
    }

    public String getToken(Context context){
        SharedPreferences prefs = context.getSharedPreferences("Premium", Context.MODE_PRIVATE);
        String token = prefs.getString("Token", null);
        return token;
    }

    public String getUserId(Context context){
        SharedPreferences prefs = context.getSharedPreferences("Cement", Context.MODE_PRIVATE);
        String token = prefs.getString("user_id", null);
        return token;
    }

    public LoginResponse getUserData(Context context){
        SharedPreferences prefs = context.getSharedPreferences("Premium", Context.MODE_PRIVATE);
        LoginResponse loginResponse = new LoginResponse();
        loginResponse.setUserId(PremiumEngine.myInstance.convertStringToInt(prefs.getString("user_id", null)));
        loginResponse.setEmail(prefs.getString("Role", null));
        loginResponse.setUsername(prefs.getString("Username", null));
        loginResponse.setRememberToken(prefs.getString("Token", null));
        loginResponse.setCompanyId(prefs.getString("CompanyId", null));
        loginResponse.setSsDefaultlocId(prefs.getString("LocationId", null));
        loginResponse.setSsDefaultorgId(prefs.getString("OrgId", null));
        loginResponse.setFirstName(prefs.getString("first_name", null));
        loginResponse.setLastName(prefs.getString("last_name", null));
        loginResponse.setEmpId(PremiumEngine.myInstance.convertStringToInt(prefs.getString("emp_id", null)));
        return loginResponse;
    }


    public void logoutSession(Context context){
        SharedPreferences sharedpreferences = context.getSharedPreferences("Premium", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear();
        editor.commit();
    }

    public IDName getTownData(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("Premium", Context.MODE_PRIVATE);
        IDName idName = new IDName();
        idName.setId(prefs.getInt("city_id", 0));
        idName.setName(prefs.getString("city_name", null));
        return idName;
    }

    public IDName getBeatId(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("Premium", Context.MODE_PRIVATE);
        IDName idName = new IDName();
        idName.setId(prefs.getInt("beat_id", 0));
        idName.setName(prefs.getString("beat_name", null));
        return idName;
    }

    public IDName getStockist(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("Premium", Context.MODE_PRIVATE);
        IDName idName = new IDName();
        idName.setId(prefs.getInt("city_id", 0));
        idName.setName(prefs.getString("city_name", null));
        return idName;
    }

    public IDName getCustType(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("Premium", Context.MODE_PRIVATE);
        IDName idName = new IDName();
        idName.setId(prefs.getInt("customer_type_id", 0));
        idName.setName(prefs.getString("customer_type", null));
        return idName;
    }

    public IDName getOutLet(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("Premium", Context.MODE_PRIVATE);
        IDName idName = new IDName();
        idName.setId(prefs.getInt("customer_id", 0));
        idName.setName(prefs.getString("customer_site_name", null));
        return idName;
    }

    public void setCustType(Context context, int townID,String townName){
        SharedPreferences.Editor editor = context.getSharedPreferences("Premium", Context.MODE_PRIVATE).edit();
        editor.putInt("customer_type_id", townID);
        editor.putString("customer_type", townName);
        editor.commit();
    }

    public void setOutLet(Context context, int townID,String townName){
        SharedPreferences.Editor editor = context.getSharedPreferences("Premium", Context.MODE_PRIVATE).edit();
        editor.putInt("customer_id", townID);
        editor.putString("customer_site_name", townName);
        editor.commit();
    }

    public void setStockist(Context context, int townID,String townName){
        SharedPreferences.Editor editor = context.getSharedPreferences("Premium", Context.MODE_PRIVATE).edit();
        editor.putInt("customer_site_id", townID);
        editor.putString("customer_site_name", townName);
        editor.commit();
    }

    public void setSaleTown(Context context, int townID,String townName){
        SharedPreferences.Editor editor = context.getSharedPreferences("Premium", Context.MODE_PRIVATE).edit();
        editor.putInt("city_id", townID);
        editor.putString("city_name", townName);
        editor.commit();
    }

    public void setBeatID(Context context, int beatID,String beatName){
        SharedPreferences.Editor editor = context.getSharedPreferences("Premium", Context.MODE_PRIVATE).edit();
        editor.putInt("beat_id", beatID);
        editor.putString("beat_name", beatName);
        editor.commit();
    }

    public void setStartEmployeeWorkTime(String calenderTime,Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences("Muktha", Context.MODE_PRIVATE).edit();
        editor.putString("startTime", calenderTime);
        editor.commit();
    }

    public void setEndEmployeeWorkTime(String calenderTime,Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences("Muktha", Context.MODE_PRIVATE).edit();
        editor.putString("endTime", calenderTime);
        editor.commit();
    }

    public String getStartTime(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("Muktha", Context.MODE_PRIVATE);
        return prefs.getString("startTime", null);
    }

    public String getEndTime(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("Muktha", Context.MODE_PRIVATE);
        return prefs.getString("endTime", null);
    }
}
