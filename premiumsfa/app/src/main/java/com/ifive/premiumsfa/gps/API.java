package com.ifive.premiumsfa.gps;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Comp11 on 1/6/2018.
 */
public interface API {

    @POST("premiumcapp/client/premiumcuatprod/public/mobileapputility/loadtrackingdetails")
    Call<SuccessMessage> updateGeoLocation(@Body GeoLocation geoLocation);
}