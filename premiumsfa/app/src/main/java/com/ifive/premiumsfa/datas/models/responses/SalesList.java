package com.ifive.premiumsfa.datas.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Syss4 on 2/17/2018.
 */

public class SalesList {

    @SerializedName("ar_sales_hdr_id")
    @Expose
    private String arSalesHdrId;
    @SerializedName("sales_order_no")
    @Expose
    private String salesOrderNo;
    @SerializedName("sales_order_date")
    @Expose
    private String salesOrderDate;
    @SerializedName("sales_order_type_id")
    @Expose
    private String salesOrderTypeId;
    @SerializedName("sales_order_type")
    @Expose
    private Object salesOrderType;
    @SerializedName("sales_order_source")
    @Expose
    private String salesOrderSource;
    @SerializedName("ar_frieghtcarriers_hdr_id")
    @Expose
    private String arFrieghtcarriersHdrId;
    @SerializedName("carrier_name")
    @Expose
    private Object carrierName;
    @SerializedName("salesperson_id")
    @Expose
    private String salespersonId;
    @SerializedName("salesperson_name")
    @Expose
    private String salespersonName;
    @SerializedName("bill_to_customer_id")
    @Expose
    private String billToCustomerId;
    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @SerializedName("organization_id")
    @Expose
    private String organizationId;
    @SerializedName("organization_name")
    @Expose
    private String organizationName;
    @SerializedName("order_status_id")
    @Expose
    private String orderStatusId;
    @SerializedName("order_total")
    @Expose
    private String orderTotal;
    @SerializedName("createdby_id")
    @Expose
    private Object createdbyId;
    @SerializedName("created_by")
    @Expose
    private Object createdBy;
    @SerializedName("remarks")
    @Expose
    private Object remarks;
    @SerializedName("invoice_flag")
    @Expose
    private String invoiceFlag;

    public String getArSalesHdrId() {
        return arSalesHdrId;
    }

    public void setArSalesHdrId(String arSalesHdrId) {
        this.arSalesHdrId = arSalesHdrId;
    }

    public String getSalesOrderNo() {
        return salesOrderNo;
    }

    public void setSalesOrderNo(String salesOrderNo) {
        this.salesOrderNo = salesOrderNo;
    }

    public String getSalesOrderDate() {
        return salesOrderDate;
    }

    public void setSalesOrderDate(String salesOrderDate) {
        this.salesOrderDate = salesOrderDate;
    }

    public String getSalesOrderTypeId() {
        return salesOrderTypeId;
    }

    public void setSalesOrderTypeId(String salesOrderTypeId) {
        this.salesOrderTypeId = salesOrderTypeId;
    }

    public Object getSalesOrderType() {
        return salesOrderType;
    }

    public void setSalesOrderType(Object salesOrderType) {
        this.salesOrderType = salesOrderType;
    }

    public String getSalesOrderSource() {
        return salesOrderSource;
    }

    public void setSalesOrderSource(String salesOrderSource) {
        this.salesOrderSource = salesOrderSource;
    }

    public String getArFrieghtcarriersHdrId() {
        return arFrieghtcarriersHdrId;
    }

    public void setArFrieghtcarriersHdrId(String arFrieghtcarriersHdrId) {
        this.arFrieghtcarriersHdrId = arFrieghtcarriersHdrId;
    }

    public Object getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(Object carrierName) {
        this.carrierName = carrierName;
    }

    public String getSalespersonId() {
        return salespersonId;
    }

    public void setSalespersonId(String salespersonId) {
        this.salespersonId = salespersonId;
    }

    public String getSalespersonName() {
        return salespersonName;
    }

    public void setSalespersonName(String salespersonName) {
        this.salespersonName = salespersonName;
    }

    public String getBillToCustomerId() {
        return billToCustomerId;
    }

    public void setBillToCustomerId(String billToCustomerId) {
        this.billToCustomerId = billToCustomerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getOrderStatusId() {
        return orderStatusId;
    }

    public void setOrderStatusId(String orderStatusId) {
        this.orderStatusId = orderStatusId;
    }

    public String getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(String orderTotal) {
        this.orderTotal = orderTotal;
    }

    public Object getCreatedbyId() {
        return createdbyId;
    }

    public void setCreatedbyId(Object createdbyId) {
        this.createdbyId = createdbyId;
    }

    public Object getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Object createdBy) {
        this.createdBy = createdBy;
    }

    public Object getRemarks() {
        return remarks;
    }

    public void setRemarks(Object remarks) {
        this.remarks = remarks;
    }

    public String getInvoiceFlag() {
        return invoiceFlag;
    }

    public void setInvoiceFlag(String invoiceFlag) {
        this.invoiceFlag = invoiceFlag;
    }

}
