package com.ifive.premiumsfa.datas.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Syss4 on 2/15/2018.
 */

public class OutletList {

    @SerializedName("customer_site_id")
    @Expose
    private Integer customerSiteId;
    @SerializedName("customer_site_name")
    @Expose
    private String customerSiteName;
    @SerializedName("customer_id")
    @Expose
    private Integer customerId;
    @SerializedName("customer_site_number")
    @Expose
    private String customerSiteNumber;
    @SerializedName("site_type")
    @Expose
    private String siteType;
    @SerializedName("address1")
    @Expose
    private String address1;
    @SerializedName("street")
    @Expose
    private String street;
    @SerializedName("city")
    @Expose
    private Object city;
    @SerializedName("area")
    @Expose
    private String area;
    @SerializedName("state")
    @Expose
    private Object state;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("contact_header_id")
    @Expose
    private String contactHeaderId;
    @SerializedName("assigned_person")
    @Expose
    private String assignedPerson;
    @SerializedName("org_location_code")
    @Expose
    private String orgLocationCode;
    @SerializedName("active")
    @Expose
    private String active;
    @SerializedName("primary_address")
    @Expose
    private String primaryAddress;
    @SerializedName("legacy_reference1")
    @Expose
    private Object legacyReference1;
    @SerializedName("legacy_reference2")
    @Expose
    private Object legacyReference2;
    @SerializedName("legacy_reference3")
    @Expose
    private Object legacyReference3;
    @SerializedName("legacy_reference4")
    @Expose
    private Object legacyReference4;
    @SerializedName("legacy_reference5")
    @Expose
    private Object legacyReference5;
    @SerializedName("pincode")
    @Expose
    private String pincode;
    @SerializedName("cst_no")
    @Expose
    private String cstNo;
    @SerializedName("gst_no")
    @Expose
    private String gstNo;
    @SerializedName("gst_provisional_id")
    @Expose
    private String gstProvisionalId;
    @SerializedName("service_tax")
    @Expose
    private String serviceTax;
    @SerializedName("uin_no")
    @Expose
    private String uinNo;
    @SerializedName("tan_no")
    @Expose
    private String tanNo;
    @SerializedName("esi_registration")
    @Expose
    private String esiRegistration;
    @SerializedName("pf_registration_no")
    @Expose
    private String pfRegistrationNo;
    @SerializedName("website_address")
    @Expose
    private String websiteAddress;
    @SerializedName("company_additional_info")
    @Expose
    private String companyAdditionalInfo;
    @SerializedName("pan_no")
    @Expose
    private String panNo;
    @SerializedName("created_by")
    @Expose
    private Object createdBy;
    @SerializedName("created_date")
    @Expose
    private Object createdDate;
    @SerializedName("last_updated_by")
    @Expose
    private Object lastUpdatedBy;
    @SerializedName("last_updated_date")
    @Expose
    private Object lastUpdatedDate;

    public Integer getCustomerSiteId() {
        return customerSiteId;
    }

    public void setCustomerSiteId(Integer customerSiteId) {
        this.customerSiteId = customerSiteId;
    }

    public String getCustomerSiteName() {
        return customerSiteName;
    }

    public void setCustomerSiteName(String customerSiteName) {
        this.customerSiteName = customerSiteName;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getCustomerSiteNumber() {
        return customerSiteNumber;
    }

    public void setCustomerSiteNumber(String customerSiteNumber) {
        this.customerSiteNumber = customerSiteNumber;
    }

    public String getSiteType() {
        return siteType;
    }

    public void setSiteType(String siteType) {
        this.siteType = siteType;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Object getCity() {
        return city;
    }

    public void setCity(Object city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Object getState() {
        return state;
    }

    public void setState(Object state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getContactHeaderId() {
        return contactHeaderId;
    }

    public void setContactHeaderId(String contactHeaderId) {
        this.contactHeaderId = contactHeaderId;
    }

    public String getAssignedPerson() {
        return assignedPerson;
    }

    public void setAssignedPerson(String assignedPerson) {
        this.assignedPerson = assignedPerson;
    }

    public String getOrgLocationCode() {
        return orgLocationCode;
    }

    public void setOrgLocationCode(String orgLocationCode) {
        this.orgLocationCode = orgLocationCode;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getPrimaryAddress() {
        return primaryAddress;
    }

    public void setPrimaryAddress(String primaryAddress) {
        this.primaryAddress = primaryAddress;
    }

    public Object getLegacyReference1() {
        return legacyReference1;
    }

    public void setLegacyReference1(Object legacyReference1) {
        this.legacyReference1 = legacyReference1;
    }

    public Object getLegacyReference2() {
        return legacyReference2;
    }

    public void setLegacyReference2(Object legacyReference2) {
        this.legacyReference2 = legacyReference2;
    }

    public Object getLegacyReference3() {
        return legacyReference3;
    }

    public void setLegacyReference3(Object legacyReference3) {
        this.legacyReference3 = legacyReference3;
    }

    public Object getLegacyReference4() {
        return legacyReference4;
    }

    public void setLegacyReference4(Object legacyReference4) {
        this.legacyReference4 = legacyReference4;
    }

    public Object getLegacyReference5() {
        return legacyReference5;
    }

    public void setLegacyReference5(Object legacyReference5) {
        this.legacyReference5 = legacyReference5;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getCstNo() {
        return cstNo;
    }

    public void setCstNo(String cstNo) {
        this.cstNo = cstNo;
    }

    public String getGstNo() {
        return gstNo;
    }

    public void setGstNo(String gstNo) {
        this.gstNo = gstNo;
    }

    public String getGstProvisionalId() {
        return gstProvisionalId;
    }

    public void setGstProvisionalId(String gstProvisionalId) {
        this.gstProvisionalId = gstProvisionalId;
    }

    public String getServiceTax() {
        return serviceTax;
    }

    public void setServiceTax(String serviceTax) {
        this.serviceTax = serviceTax;
    }

    public String getUinNo() {
        return uinNo;
    }

    public void setUinNo(String uinNo) {
        this.uinNo = uinNo;
    }

    public String getTanNo() {
        return tanNo;
    }

    public void setTanNo(String tanNo) {
        this.tanNo = tanNo;
    }

    public String getEsiRegistration() {
        return esiRegistration;
    }

    public void setEsiRegistration(String esiRegistration) {
        this.esiRegistration = esiRegistration;
    }

    public String getPfRegistrationNo() {
        return pfRegistrationNo;
    }

    public void setPfRegistrationNo(String pfRegistrationNo) {
        this.pfRegistrationNo = pfRegistrationNo;
    }

    public String getWebsiteAddress() {
        return websiteAddress;
    }

    public void setWebsiteAddress(String websiteAddress) {
        this.websiteAddress = websiteAddress;
    }

    public String getCompanyAdditionalInfo() {
        return companyAdditionalInfo;
    }

    public void setCompanyAdditionalInfo(String companyAdditionalInfo) {
        this.companyAdditionalInfo = companyAdditionalInfo;
    }

    public String getPanNo() {
        return panNo;
    }

    public void setPanNo(String panNo) {
        this.panNo = panNo;
    }

    public Object getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Object createdBy) {
        this.createdBy = createdBy;
    }

    public Object getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Object createdDate) {
        this.createdDate = createdDate;
    }

    public Object getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(Object lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public Object getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(Object lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

}
