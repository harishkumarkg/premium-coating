package com.ifive.premiumsfa.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ifive.premiumsfa.Movement;
import com.ifive.premiumsfa.R;
import com.ifive.premiumsfa.datas.models.responses.BeatList;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Syss4 on 2/12/2018.
 */

public class BeatListAdapter extends RecyclerView.Adapter<BeatListAdapter.MyViewHolder> {

    private Context context;
    private List<BeatList> cartList;
    private ArrayList<BeatList> townBeatLists;
    Movement movement;
    int beatID;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout viewBackground, viewForeground;
        public TextView beatName, serialNumber;
        public MyViewHolder(View view) {
            super(view);
            beatName = view.findViewById(R.id.beat_name);
            serialNumber = view.findViewById(R.id.serial_number);
            viewForeground = view.findViewById(R.id.view_foreground);
        }
    }

    public BeatListAdapter(Context context, List<BeatList> cartList, Movement movement,int beatID) {
        this.context = context;
        this.cartList = cartList;
        this.movement = movement;
        this.beatID = beatID;
        this.townBeatLists = new ArrayList<BeatList>();
        this.townBeatLists.addAll(cartList);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.beat_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final BeatList item = cartList.get(position);
        holder.beatName.setText(item.getBeatName());
        holder.serialNumber.setText(position+1+"");
        holder.viewForeground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                movement.setSaleBeatPreference(item.getBeatId(),item.getBeatName());
            }
        });
        if((beatID != 0) && (beatID == item.getBeatId())){
            holder.viewForeground.setBackground(context.getResources().getDrawable(R.drawable.green_bg));
            holder.beatName.setTextColor(context.getResources().getColor(R.color.white));
        }else{
            holder.viewForeground.setBackground(context.getResources().getDrawable(R.drawable.gradient_bg2));
            holder.beatName.setTextColor(context.getResources().getColor(R.color.black_low));
        }
    }

    @Override
    public int getItemCount() {
        return cartList.size();
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        cartList.clear();
        if (charText.length() == 0) {
            cartList.addAll(townBeatLists);
        }else{
            for (BeatList wp : townBeatLists){
                if (wp.getBeatName().toLowerCase(Locale.getDefault()).contains(charText)){
                    cartList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}
