package com.ifive.premiumsfa.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ifive.premiumsfa.R;
import com.ifive.premiumsfa.adapter.AttendanceAdapter;
import com.ifive.premiumsfa.adapter.SalesOrderAdapter;
import com.ifive.premiumsfa.datas.SessionManager;
import com.ifive.premiumsfa.datas.models.requests.AttendanceListRequest;
import com.ifive.premiumsfa.datas.models.responses.AttendListModel;
import com.ifive.premiumsfa.datas.models.responses.SalesList;
import com.ifive.premiumsfa.datas.remote.UserAPICall;
import com.ifive.premiumsfa.engine.PremiumEngine;
import com.ifive.premiumsfa.engine.RetroFitEngine;
import com.ifive.premiumsfa.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by iftpc011 on 22/3/18.
 */

public class AttendanceList extends BaseActivity {

    private ProgressDialog pDialog;
    SessionManager sessionManager;
    private AttendanceAdapter attendanceAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    Context myContext;
    AttendanceListRequest attendanceListRequest;

    List<AttendListModel> attendListModels;
    AttendListModel attendListModel;
    @BindView(R.id.gatepass_swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.sales_message)
    TextView messageDisplay;
    @BindView(R.id.orderList)
    RecyclerView gatepass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.orderlist);
        pDialog = PremiumEngine.myInstance.getProgDialog(this);
        ButterKnife.bind(this);
        sessionManager = new SessionManager();
        getAttList();
    }

    public void getAttList() {
        if (PremiumEngine.isNetworkAvailable(this)) {
            pDialog.show();
            UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);
            attendanceListRequest = new AttendanceListRequest();
            attendanceListRequest.setUserId(/*sessionManager.getUserId(myContext)*/15);
            Call<AttendListModel> callEnqueue = userAPICall.listAtten(sessionManager.getToken(this),attendanceListRequest);
            callEnqueue.enqueue(new Callback<AttendListModel>() {
                @Override
                public void onResponse(Call<AttendListModel> call, Response<AttendListModel> response) {
                    attendListModel = response.body();
                    /*if(attendListModel !=null){
                        pageNo = attendListModel.getPagination().getNextPage();
                    }*/
                    setItemRecyclerView();
                    pDialog.dismiss();
                }
                @Override
                public void onFailure(Call<AttendListModel> call, Throwable t) {
                    Toast.makeText(AttendanceList.this,  t.getMessage(), Toast.LENGTH_SHORT).show();
                    pDialog.dismiss();
                }
            });
        } else {
            PremiumEngine.myInstance.snackbarNoInternet(this);
        }
    }

    private void setItemRecyclerView() {
        if(attendListModel != null){
            attendanceAdapter = new AttendanceAdapter(this, attendListModels,this);
            gatepass.setAdapter(attendanceAdapter);
            gatepass.setItemAnimator(new DefaultItemAnimator());
            gatepass.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        }
    }


    public void onRefresh() {
        getAttList();
    }
}
