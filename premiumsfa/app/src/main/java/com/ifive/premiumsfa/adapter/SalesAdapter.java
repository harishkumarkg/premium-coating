package com.ifive.premiumsfa.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ifive.premiumsfa.Movement;
import com.ifive.premiumsfa.R;
import com.ifive.premiumsfa.datas.models.requests.SoItemList;

import java.util.List;

/**
 * Created by Syss4 on 2/14/2018.
 */

public class SalesAdapter extends RecyclerView.Adapter<SalesAdapter.MyViewHolder> {
    private Context context;
    private List<SoItemList> cartList;
    private Movement salesOrderFragment;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout viewBackground, viewForeground;
        public TextView productName, unitPrice, qty,gst, total, serialNumber, uom, proSize;
        public MyViewHolder(View view) {
            super(view);
            productName = view.findViewById(R.id.product_name);
            unitPrice = view.findViewById(R.id.pro_price);
            qty = view.findViewById(R.id.pro_qty);
            gst = view.findViewById(R.id.pro_gst);
            total = view.findViewById(R.id.pro_total);
            serialNumber = view.findViewById(R.id.serial_number);
            uom  = view.findViewById(R.id.pro_uom);
            proSize  = view.findViewById(R.id.pro_sizes);
            viewBackground = view.findViewById(R.id.view_background);
            viewForeground = view.findViewById(R.id.view_foreground);
        }
    }

    public SalesAdapter(Context context, List<SoItemList> cartList, Movement movement) {
        this.context = context;
        this.cartList = cartList;
        this.salesOrderFragment = salesOrderFragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sales_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final SoItemList item = cartList.get(position);
        holder.productName.setText(item.getProductName());
        holder.unitPrice.setText(item.getUnitPrice());
        holder.qty.setText(item.getQty());
        holder.gst.setText(item.getGst());
        holder.uom.setText(item.getUomCodeId());
        holder.proSize.setText(item.getProSize());
        holder.total.setText(""+item.getTotal());
        holder.serialNumber.setText(position+1+"");
        holder.viewForeground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //salesOrderFragment.editItem(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return cartList.size();
    }

    public void removeItem(int position) {
        cartList.remove(position);
        salesOrderFragment.calculateTotalAmount();
        notifyItemRemoved(position);
    }

    public void restoreItem(SoItemList item, int position) {
        cartList.add(position, item);
        // notify item added by position
        notifyItemInserted(position);
    }
}
