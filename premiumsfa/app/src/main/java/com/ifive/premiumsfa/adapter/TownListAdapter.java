package com.ifive.premiumsfa.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.ifive.premiumsfa.Movement;
import com.ifive.premiumsfa.R;
import com.ifive.premiumsfa.datas.SessionManager;
import com.ifive.premiumsfa.datas.models.responses.TownList;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Comp11 on 1/30/2018.
 */

public class TownListAdapter extends RecyclerView.Adapter<TownListAdapter.MyViewHolder> {
    private Context context;
    private List<TownList> cartList;
    private ArrayList<TownList> townList;
    Movement movement;
    SessionManager sessionManager;
    Integer townID;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout viewForeground;
        public TextView townName, serialNumber;
        public MyViewHolder(View view) {
            super(view);
            townName = view.findViewById(R.id.town_name);
            serialNumber = view.findViewById(R.id.serial_number);
            viewForeground = view.findViewById(R.id.view_foreground);
        }
    }

    public TownListAdapter(Context context, List<TownList> cartList, Movement movement, int townID) {
        this.context = context;
        this.cartList = cartList;
        this.movement = movement;
        this.townID = townID;
        this.townList = new ArrayList<TownList>();
        this.townList.addAll(cartList);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.town_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final TownList item = cartList.get(position);
        holder.townName.setText(item.getCityName());
        holder.serialNumber.setText(position+1+"");
        holder.viewForeground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                movement.setSaleTownPreference(item.getCityId(),item.getCityName());
            }
        });
        if((townID != 0) && (townID.equals(item.getCityId()))){
            holder.viewForeground.setBackground(context.getResources().getDrawable(R.drawable.green_bg));
            holder.townName.setTextColor(context.getResources().getColor(R.color.white));
        }else{
            holder.viewForeground.setBackground(context.getResources().getDrawable(R.drawable.gradient_bg2));
            holder.townName.setTextColor(context.getResources().getColor(R.color.black_low));
        }
    }

    @Override
    public int getItemCount() {
        return cartList.size();
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        cartList.clear();
        if (charText.length() == 0) {
            cartList.addAll(townList);
        }else{
            for (TownList wp : townList){
                if (wp.getCityName().toLowerCase(Locale.getDefault()).contains(charText)){
                    cartList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}