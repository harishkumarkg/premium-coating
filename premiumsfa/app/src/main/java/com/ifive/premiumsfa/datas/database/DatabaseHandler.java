package com.ifive.premiumsfa.datas.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import com.ifive.premiumsfa.gps.GeoLocation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Comp11 on 1/8/2018.
 */

public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "rathna_cements";
    private static final String TABLE_GPS = "gps_location";
    private static final String KEY_ID = "id";
    private static final String KEY_UID = "uid";
    private static final String KEY_LAT = "latitude";
    private static final String KEY_LON = "longitude";
    private static final String KEY_BATTERY = "battery";
    private static final String KEY_DATE = "date";
    private static final String KEY_TIME = "time";

    private static final String TABLE_MENUS = "menu_permission";
    private static final String KEY_MENU_ID = "menu_id";
    private static final String KEY_SLUG = "slug";
    private static final String KEY_MENU = "menu_name";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //3rd argument to be passed is CursorFactory instance
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_GPS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_UID + " INTEGER," +
                KEY_LAT + " TEXT," + KEY_LON + " TEXT," + KEY_DATE + " TEXT," + KEY_TIME + " TEXT,"
                + KEY_BATTERY + " TEXT" + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
        String CREATE_MENUS = "CREATE TABLE IF NOT EXISTS " + TABLE_MENUS + "("
                + KEY_MENU_ID + " INTEGER PRIMARY KEY," + KEY_SLUG + " TEXT," + KEY_MENU + " TEXT)";
        db.execSQL(CREATE_MENUS);
    }

    // code to get all contacts in a list view
    public List<GeoLocation> getAllGPSLocations() {
        List<GeoLocation> gpsLocations = new ArrayList<GeoLocation>();
        String selectQuery = "SELECT  * FROM " + TABLE_GPS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                GeoLocation gpsLocations1 = new GeoLocation();
                gpsLocations1.setUid(Integer.parseInt(cursor.getString(1)));
                gpsLocations1.setLatitude(Double.parseDouble(cursor.getString(2)));
                gpsLocations1.setLongitude(Double.parseDouble(cursor.getString(3)));
                gpsLocations1.setDate(cursor.getString(4));
                gpsLocations1.setTime(cursor.getString(5));
                gpsLocations1.setBattery(Integer.parseInt(cursor.getString(6)));
                gpsLocations.add(gpsLocations1);
            } while (cursor.moveToNext());
        }
        return gpsLocations;
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GPS);

        // Create tables again
        onCreate(db);
    }

    // code to add the new GPSLocations
    public void addGPSLocation(GeoLocation GPSLocations) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_UID, GPSLocations.getUid());
        values.put(KEY_LAT, GPSLocations.getLatitude());
        values.put(KEY_LON, GPSLocations.getLongitude());
        values.put(KEY_BATTERY, GPSLocations.getBattery());
        values.put(KEY_DATE, GPSLocations.getDate());
        values.put(KEY_TIME, GPSLocations.getTime());
        db.insert(TABLE_GPS, null, values);
        db.close(); // Closing database connection
    }

    // Upgrading database
    public void truncateGPSLocation() {
        // Drop older table if existed
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_GPS);

        // Create tables again
        onCreate(db);
    }


    // code to get the single contact
//    public GeoLocation getGPSLocation(int id) {
//        SQLiteDatabase db = this.getReadableDatabase();
//
//        Cursor cursor = db.query(TABLE_GPS, new String[] { KEY_ID,
//                        KEY_LAT, KEY_LON}, KEY_ID + "=?",
//                new String[] { String.valueOf(id) }, null, null, null, null);
//        if (cursor != null)
//            cursor.moveToFirst();
//
//        GeoLocation gpsLocations = new GeoLocation(Integer.parseInt(cursor.getString(0)),
//                cursor.getString(1), cursor.getString(2));
//        // return GPSLocations
//        return gpsLocations;
//    }

    // code to update the single GPSLocations
//    public int updateGPSLocation(GeoLocation gpsLocations1) {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        values.put(KEY_LAT, gpsLocations1.getLat());
//        values.put(KEY_LON, gpsLocations1.getLon());
//
//        // updating row
//        return db.update(TABLE_GPS, values, KEY_ID + " = ?",
//                new String[] { String.valueOf(gpsLocations1.getID()) });
//    }

    // Deleting single GPSLocations
//    public void deleteGPSLocation(GeoLocation gpsLocations1) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        db.delete(TABLE_GPS, KEY_ID + " = ?",
//                new String[] { String.valueOf(gpsLocations1.getID()) });
//        db.close();
//    }

    // Getting contacts Count
//    public int getGPSLocationCount() {
//        String countQuery = "SELECT  * FROM " + TABLE_GPS;
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor = db.rawQuery(countQuery, null);
//        cursor.close();
//
//        // return count
//        return cursor.getCount();
//    }

}