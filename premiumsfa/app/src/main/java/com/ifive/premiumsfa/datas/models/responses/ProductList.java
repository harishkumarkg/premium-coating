package com.ifive.premiumsfa.datas.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Syss4 on 2/14/2018.
 */

public class ProductList {

    @SerializedName("concat_segment")
    @Expose
    private String concatSegment;
    @SerializedName("hsn_code")
    @Expose
    private String hsnCode;
    @SerializedName("display_name")
    @Expose
    private String displayName;
    @SerializedName("taxgroup_name")
    @Expose
    private String taxgroupName;
    @SerializedName("company_id")
    @Expose
    private String companyId;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("uom_code_id")
    @Expose
    private String uomCodeId;

    public String getConcatSegment() {
        return concatSegment;
    }

    public void setConcatSegment(String concatSegment) {
        this.concatSegment = concatSegment;
    }

    public String getHsnCode() {
        return hsnCode;
    }

    public void setHsnCode(String hsnCode) {
        this.hsnCode = hsnCode;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getTaxgroupName() {
        return taxgroupName;
    }

    public void setTaxgroupName(String taxgroupName) {
        this.taxgroupName = taxgroupName;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUomCodeId() {
        return uomCodeId;
    }

    public void setUomCodeId(String uomCodeId) {
        this.uomCodeId = uomCodeId;
    }
}
