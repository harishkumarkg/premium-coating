package com.ifive.premiumsfa;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.ifive.premiumsfa.datas.SessionManager;
import com.ifive.premiumsfa.datas.remote.UserAPICall;
import com.ifive.premiumsfa.engine.PremiumEngine;
import com.ifive.premiumsfa.engine.RetroFitEngine;
import com.ifive.premiumsfa.ui.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Syss4 on 2/8/2018.
 */

public class Collection_List extends BaseActivity {

    @BindView(R.id.col_list)
    RecyclerView collictionList;
    private ProgressDialog pDialog;
    SessionManager sessionManager;
    Context myContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.collection_list);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(PremiumEngine.isNetworkAvailable(myContext)){
           // getSalesList();
        }else{
            PremiumEngine.myInstance.snackbarNoInternet(myContext);
        }
    }

   /* public void getSalesList() {
        pDialog.show();
        salesListResponses = new ArrayList<>();
        UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);
        Call<List<SalesListResponse>> callEnqueue = userAPICall.getSalesList(sessionManager.getToken(myContext));
        callEnqueue.enqueue(new Callback<List<SalesListResponse>>() {
            @Override
            public void onResponse(Call<List<SalesListResponse>> call, Response<List<SalesListResponse>> response) {
                salesListResponses = response.body();
                if(salesListResponses != null){
//                    Toast.makeText(myContext,
//                            "Size of List = "+salesListResponses.size(), Toast.LENGTH_SHORT).show();
                    setItemRecyclerView(salesListResponses);
                }
                pDialog.dismiss();
            }
            @Override
            public void onFailure(Call<List<SalesListResponse>> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(myContext,  t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setItemRecyclerView(List<SalesListResponse> salesListResponses) {
        salesListAdapter = new SalesListAdapter(myContext, salesListResponses,this);
        salesDataList.setAdapter(salesListAdapter);
        mLayoutManager = new LinearLayoutManager(myContext);
        salesDataList.setLayoutManager(mLayoutManager);
        salesDataList.setItemAnimator(new DefaultItemAnimator());
        salesDataList.addItemDecoration(new DividerItemDecoration(myContext, DividerItemDecoration.VERTICAL));
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void openSale(int position,SalesListResponse salesListResponse) {
        CollectionRequest collectionRequest = new CollectionRequest();
        collectionRequest.setCollectionId(salesListResponse.getCollectionId());
        pDialog.show();
        collectionProducts = new ArrayList<>();
        UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);
        Call<List<CollectionProducts>> callEnqueue = userAPICall.getCollectionProducts(sessionManager.getToken(myContext),collectionRequest);
        callEnqueue.enqueue(new Callback<List<CollectionProducts>>() {
            @Override
            public void onResponse(Call<List<CollectionProducts>> call, Response<List<CollectionProducts>> response) {
                collectionProducts = response.body();
                if(collectionProducts != null){
                    showItems(collectionProducts);
                }
                pDialog.dismiss();
            }
            @Override
            public void onFailure(Call<List<CollectionProducts>> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(myContext,  t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void showItems(List<CollectionProducts> collectionProducts) {
        View addItemView = LayoutInflater.from(myContext)
                .inflate(R.layout.recycler_list, null);
        android.app.AlertDialog.Builder chartDialog = new android.app.AlertDialog.Builder(myContext);
        chartDialog.setView(addItemView);
        chartDialog.setNegativeButton("Close", new DialogInterface.OnClickListener(){

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }

        });
        final android.app.AlertDialog chartAlertDialog = chartDialog.show();
        RecyclerView itemsDataList =  addItemView.findViewById(R.id.items_data_list);
        TextView textTitle =  addItemView.findViewById(R.id.text_title);
        textTitle.setText("Items List");
        ItemShowAdapter itemShowAdapter = new ItemShowAdapter(myContext, collectionProducts);
        itemsDataList.setAdapter(itemShowAdapter);
        mLayoutManager = new LinearLayoutManager(myContext);
        itemsDataList.setLayoutManager(mLayoutManager);
        itemsDataList.setItemAnimator(new DefaultItemAnimator());
        itemsDataList.addItemDecoration(new DividerItemDecoration(myContext, DividerItemDecoration.VERTICAL));
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }*/
}
