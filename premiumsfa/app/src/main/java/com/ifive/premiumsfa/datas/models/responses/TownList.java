package com.ifive.premiumsfa.datas.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Syss4 on 2/10/2018.
 */

public class TownList {

    @SerializedName("city_id")
    @Expose
    private Integer cityId;
    @SerializedName("country_id")
    @Expose
    private String countryId;
    @SerializedName("state_id")
    @Expose
    private String stateId;
    @SerializedName("city_name")
    @Expose
    private String cityName;
    @SerializedName("created_by")
    @Expose
    private Object createdBy;
    @SerializedName("created_date")
    @Expose
    private Object createdDate;
    @SerializedName("last_updated_by")
    @Expose
    private Object lastUpdatedBy;
    @SerializedName("last_updated_date")
    @Expose
    private Object lastUpdatedDate;

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public Object getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Object createdBy) {
        this.createdBy = createdBy;
    }

    public Object getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Object createdDate) {
        this.createdDate = createdDate;
    }

    public Object getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(Object lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public Object getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(Object lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

}
