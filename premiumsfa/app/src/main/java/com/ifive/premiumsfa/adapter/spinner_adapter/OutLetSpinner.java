package com.ifive.premiumsfa.adapter.spinner_adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ifive.premiumsfa.datas.models.responses.OutletList;
import com.ifive.premiumsfa.datas.models.responses.ProductList;

import java.util.List;

/**
 * Created by Syss4 on 2/17/2018.
 */

public class OutLetSpinner extends ArrayAdapter<OutletList> {

    private Context context;
    // Your custom values for the spinner (User)
    private List<OutletList> values;

    public OutLetSpinner(Context context, int textViewResourceId,
                                     List<OutletList> values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public int getCount(){
        return values.size();
    }

    @Override
    public OutletList getItem(int position){
        return values.get(position);
    }

    @Override
    public long getItemId(int position){
        return position;
    }

    // And the "magic" goes here
    // This is for the "passive" state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
        TextView label = new TextView(context);
        label.setTextColor(Color.BLACK);
        label.setTextSize(15);
        label.setPadding(10,10,10,10);
        // Then you can get the current item using the values array (Users array) and the current position
        // You can NOW reference each method you has created in your bean object (User class)
        label.setText(values.get(position).getCustomerSiteName());
        // And finally return your dynamic (or custom) view for each spinner item
        return label;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = new TextView(context);
        label.setTextColor(Color.BLACK);
        label.setTextSize(15);
        label.setText(values.get(position).getCustomerSiteName());
        label.setPadding(10,10,10,10);
        return label;
    }
}