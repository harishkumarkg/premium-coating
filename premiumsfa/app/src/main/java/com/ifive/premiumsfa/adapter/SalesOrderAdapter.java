package com.ifive.premiumsfa.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.ifive.premiumsfa.OrderList;
import com.ifive.premiumsfa.R;
import com.ifive.premiumsfa.datas.models.responses.SalesList;

import java.util.List;

/**
 * Created by Syss4 on 2/17/2018.
 */

public class SalesOrderAdapter extends RecyclerView.Adapter<SalesOrderAdapter.MyViewHolder> {

    private Context context;
    private List<SalesList> salesLists;
    private OrderList orderList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView orderId,orderDate, customerName, sNo;

        public MyViewHolder(View view) {
            super(view);
            sNo = view.findViewById(R.id.sales_serial_number);
            orderId = view.findViewById(R.id.soId);
            orderDate = view.findViewById(R.id.salesDate);
            customerName = view.findViewById(R.id.companyName);
        }
    }

    public SalesOrderAdapter(Context context, List<SalesList> cartList, OrderList orderList) {
        this.context = context;
        this.salesLists = cartList;
        this.orderList = orderList;
    }

    @Override
    public SalesOrderAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.orderlist_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SalesOrderAdapter.MyViewHolder holder, int position) {

        final SalesList item = salesLists.get(position);
        holder.sNo.setText(position+1+"");
        holder.orderId.setText(item.getSalesOrderNo());
        holder.orderDate.setText(item.getSalesOrderDate());
        holder.customerName.setText(item.getCustomerName());

    }

    @Override
    public int getItemCount() {
        return salesLists.size();
    }
}
