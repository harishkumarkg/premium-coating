package com.ifive.premiumsfa.ui.base;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.ifive.premiumsfa.Attendance;
import com.ifive.premiumsfa.Collection_List;
import com.ifive.premiumsfa.CompanyList;
import com.ifive.premiumsfa.MainActivity;
import com.ifive.premiumsfa.Movement;
import com.ifive.premiumsfa.OrderList;
import com.ifive.premiumsfa.R;
import com.ifive.premiumsfa.Target;
import com.ifive.premiumsfa.datas.SessionManager;
import com.ifive.premiumsfa.ui.AttendanceList;
import com.ifive.premiumsfa.ui.ContractorDCR;
import com.ifive.premiumsfa.ui.Expenses;
import com.ifive.premiumsfa.ui.LeaveEntry;
import com.ifive.premiumsfa.ui.login.LoginActivity;

public abstract class BaseActivity extends AppCompatActivity implements MenuItem.OnMenuItemClickListener {
    private FrameLayout view_stub; //This is the framelayout to keep your content view
    private NavigationView navigation_view; // The new navigation view from Android Design Library. Can inflate menu resources. Easy
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private Menu drawerMenu;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.drawer_layout);// The base layout that contains your navigation drawer.
        view_stub = findViewById(R.id.view_stub);
        navigation_view = findViewById(R.id.navigation_view);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                0, 0);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        navigation_view.setItemIconTintList(null);
        sessionManager = new SessionManager();
        drawerMenu = navigation_view.getMenu();
        for (int i = 0; i < drawerMenu.size(); i++) {
            drawerMenu.getItem(i).setOnMenuItemClickListener(this);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void setContentView(int layoutResID) {
        if (view_stub != null) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            View stubView = inflater.inflate(layoutResID, view_stub, false);
            view_stub.addView(stubView, lp);
        }
    }

    @Override
    public void setContentView(View view) {
        if (view_stub != null) {
            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            view_stub.addView(view, lp);
        }
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        if (view_stub != null) {
            view_stub.addView(view, params);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.home:
                startActivity(new Intent(this, MainActivity.class));
                break;

            case R.id.attendance:
                startActivity(new Intent(this, Attendance.class));
                break;

            case R.id.attendanceHist:
                startActivity(new Intent(this, AttendanceList.class));
                break;

            case R.id.movements:
                startActivity(new Intent(this, Movement.class));
                break;

            case R.id.expenses:
                startActivity(new Intent(this, Expenses.class));
                break;

            case R.id.target:
                startActivity(new Intent(this, Target.class));
                break;

            case R.id.collections:
                startActivity(new Intent(this, Collection_List.class));
                break;

            case R.id.dcr:
                startActivity(new Intent(this, ContractorDCR.class));
                break;

            case R.id.orderlist:
                startActivity(new Intent(this, OrderList.class));
                break;

            case R.id.leaveReq:
                startActivity(new Intent(this, LeaveEntry.class));
                break;

           /* case R.id.remarks:
                startActivity(new Intent(this, CompanyList.class));
                break;*/

            case R.id.logout:
                sessionManager.logoutSession(this);
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                break;
        }
        return false;
    }
}