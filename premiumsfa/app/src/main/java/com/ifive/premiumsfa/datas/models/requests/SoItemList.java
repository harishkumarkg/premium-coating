package com.ifive.premiumsfa.datas.models.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Syss4 on 2/14/2018.
 */

public class SoItemList {

    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("hsn_code")
    @Expose
    private String hsnCode;
    @SerializedName("unit_price")
    @Expose
    private String unitPrice;
    @SerializedName("qty")
    @Expose
    private String qty;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("gst")
    @Expose
    private String gst;
    @SerializedName("total")
    @Expose
    private double total;

    @SerializedName("uom_code_id")
    @Expose
    private String uomCodeId;
    @SerializedName("gst_calculation")
    @Expose
    private String gstCalculation;
    @SerializedName("pro_size")
    @Expose
    private String proSize;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getHsnCode() {
        return hsnCode;
    }

    public void setHsnCode(String hsnCode) {
        this.hsnCode = hsnCode;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getGst() {
        return gst;
    }

    public void setGst(String gst) {
        this.gst = gst;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getUomCodeId() {
        return uomCodeId;
    }

    public void setUomCodeId(String UomCodeId) {
        this.uomCodeId = UomCodeId;
    }

    public String getGstCalculation() {
        return gstCalculation;
    }

    public void setGstCalculation(String gstCalculation) {
        this.gstCalculation = gstCalculation;
    }

    public String getProSize() {
        return proSize;
    }

    public void setProSize(String proSize) {
        this.proSize = proSize;
    }

}
