package com.ifive.premiumsfa.ui;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.wifi.hotspot2.pps.HomeSp;
import android.os.BatteryManager;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ifive.premiumsfa.MainActivity;
import com.ifive.premiumsfa.R;
import com.ifive.premiumsfa.datas.SessionManager;
import com.ifive.premiumsfa.datas.models.requests.ExpensesRequest;
import com.ifive.premiumsfa.datas.models.responses.ExpensesResponses;
import com.ifive.premiumsfa.datas.remote.UserAPICall;
import com.ifive.premiumsfa.engine.PremiumEngine;
import com.ifive.premiumsfa.engine.RetroFitEngine;
import com.ifive.premiumsfa.ui.base.BaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by Syss4 on 2/7/2018.
 */

public class Expenses extends BaseActivity {




    SessionManager sessionManager;
    EditText desc, dist, dates, f_Location, t_Location, amou, advance_t, amount_r, areas1, areas2, areas3, foodBill, foodAmount, hotelBill, hotelAmount;
    Spinner claim_typ;
    TextView allowance;
    ProgressDialog pDialog;
    Context myContext;
    ExpensesRequest expensesRequest;
    RadioGroup conveyance_radio;
    RadioButton wehType;
    private int pYear;
    private int pMonth;
    private int pDay;
    ExpensesResponses expensesResponses;
    @BindView(R.id.add_record1)
    Button send;

    LinearLayout lay1, lay2, lay3;

    static final int DATE_DIALOG_ID = 0;

    private DatePickerDialog.OnDateSetListener pDateSetListener =
            new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth) {
                    pYear = year;
                    pMonth = monthOfYear;
                    pDay = dayOfMonth;
                    updateDisplay();
                    //displayToast();
                }
            };

    /**
     * Updates the date in the TextView
     */
    private void updateDisplay() {
        dates.setText(
                new StringBuilder()
                        // Month is 0 based so add 1
                        .append(pMonth + 1).append("/")
                        .append(pDay).append("/")
                        .append(pYear).append(" "));
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.expenses);
        setTitle("Add Record");

        pDialog = PremiumEngine.myInstance.getProgDialog(this);
        ButterKnife.bind(this);
        sessionManager = new SessionManager();

        desc = (EditText) findViewById(R.id.name1);
        dist = (EditText) findViewById(R.id.number1);
        claim_typ = (Spinner) findViewById(R.id.productname1);
        dates = (EditText) findViewById(R.id.quantity1);
        f_Location = (EditText) findViewById(R.id.orderdate1);
        t_Location = (EditText) findViewById(R.id.deliverydate1);
        amou = (EditText) findViewById(R.id.totalamount1);
        advance_t = (EditText) findViewById(R.id.paidamount1);
        amount_r = (EditText) findViewById(R.id.ramount1);
        areas1 = (EditText) findViewById(R.id.area1);
        areas2 = (EditText) findViewById(R.id.area2);
        areas3 = (EditText) findViewById(R.id.area3);
        foodBill = (EditText) findViewById(R.id.bill);
        foodAmount = (EditText) findViewById(R.id.amount2);
        hotelBill = (EditText) findViewById(R.id.hotel_bil);
        hotelAmount = (EditText) findViewById(R.id.hotel_amount);
      //  send  = (Button)findViewById(R.id.add_record1);
        allowance = (TextView)findViewById(R.id.allowance_rs);
        conveyance_radio = (RadioGroup) findViewById(R.id.conveyance_group);

        lay1 = (LinearLayout)findViewById(R.id.ss1);
        lay2 = (LinearLayout)findViewById(R.id.ss2);
        lay3 = (LinearLayout)findViewById(R.id.ss3);

        List<String> categories = new ArrayList<>();

        categories.add("Local conveyance");
        categories.add("Food Allowance");
        categories.add("Accommodation");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        claim_typ.setAdapter(dataAdapter);

        claim_typ.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String item = parent.getItemAtPosition(position).toString();

                if (item.equals("Local conveyance"))
                {
                    lay1.setVisibility(View.VISIBLE);
                    lay2.setVisibility(View.GONE);
                    lay3.setVisibility(View.GONE);
                }
                else if (item.equals("Food Allowance"))
                {
                    lay2.setVisibility(View.VISIBLE);
                    lay1.setVisibility(View.GONE);
                    lay3.setVisibility(View.GONE);
                }
                else if (item.equals("Accommodation"))
                {
                    lay3.setVisibility(View.VISIBLE);
                    lay1.setVisibility(View.GONE);
                    lay2.setVisibility(View.GONE);
                }


                Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        conveyance_radio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if(checkedId == R.id.two_weel) {

                    Toast.makeText(Expenses.this.getApplicationContext(), "choice: A",
                            Toast.LENGTH_SHORT).show();
                    allowance.setText("2");
                }else if(checkedId == R.id.four_weal) {
                    Toast.makeText(Expenses.this.getApplicationContext(), "choice: B",
                            Toast.LENGTH_SHORT).show();
                    allowance.setText("4");
                }else if(checkedId == R.id.others) {
                    allowance.setText("");
                    Toast.makeText(Expenses.this.getApplicationContext(), "choice: c",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

     /*   send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(PremiumEngine.isNetworkAvailable(this)){
                    pDialog.show();
                    expensesRequest =new ExpensesRequest();
                    expensesRequest.setDate(dates.getText().toString().trim());
                    expensesRequest.setConvType(claim_typ.getSelectedItem().toString().trim());
                    expensesRequest.setAmount(allowance.getText().toString().trim());
                    expensesRequest.setLocationFrom(f_Location.getText().toString().trim());
                    expensesRequest.setLocationTo(t_Location.getText().toString().trim());
                    expensesRequest.setArea1(areas1.getText().toString().trim());
                    expensesRequest.setArea2(areas2.getText().toString().trim());
                    expensesRequest.setArea3(areas3.getText().toString().trim());
                    expensesRequest.setDiscription(desc.getText().toString().trim());
                    expensesRequest.setDistance(dist.getText().toString().trim());
                    expensesRequest.setTotalAmount(amou.getText().toString().trim());
                    expensesRequest.setAdvanceAmount(advance_t.getText().toString().trim());
                    expensesRequest.setBalanceAmount(amount_r.getText().toString().trim());
                    expensesRequest.setFoodBillno(foodBill.getText().toString().trim());
                    expensesRequest.setFoodAmount(foodAmount.getText().toString().trim());
                    expensesRequest.setHotelBillno(hotelBill.getText().toString().trim());
                    expensesRequest.setHotelAmount(hotelAmount.getText().toString().trim());

                    expensesResponses = new ExpensesResponses();
                    UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);
                    Call<ExpensesResponses> callEnqueue = userAPICall.postExpen(sessionManager.getToken(myContext), expensesRequest);
                    callEnqueue.enqueue(new Callback<ExpensesResponses>() {
                        @Override
                        public void onResponse(Call<ExpensesResponses> call, retrofit2.Response<ExpensesResponses> response) {
                            expensesResponses = response.body();
                            if(expensesResponses != null){
                                Toast.makeText(myContext,
                                        ""+expensesResponses.getMessage(), Toast.LENGTH_SHORT).show();

                                if(expensesResponses.getMessage().equals("SUCCESS")){
                                    //openIsueListFragment();
                                }

                            }
                            pDialog.dismiss();

                        }

                        @Override
                        public void onFailure(Call<ExpensesResponses> call, Throwable t) {
                            pDialog.dismiss();
                            Toast.makeText(myContext,  t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }else{
                    PremiumEngine.myInstance.snackbarNoInternet(myContext);
                }
            }
        });*/


        dates.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                showDialog(DATE_DIALOG_ID);
            }
        });

        final Calendar cal = Calendar.getInstance();
        pYear = cal.get(Calendar.YEAR);
        pMonth = cal.get(Calendar.MONTH);
        pDay = cal.get(Calendar.DAY_OF_MONTH);

        /** Display the current date in the TextView */
        updateDisplay();


    }

    @OnClick(R.id.add_record1)
    public void submitSale(View view) {
        if(PremiumEngine.isNetworkAvailable(this)){
            pDialog.show();
            pDialog.show();
            expensesRequest =new ExpensesRequest();
            expensesRequest.setDate(dates.getText().toString().trim());
            expensesRequest.setConvType(claim_typ.getSelectedItem().toString().trim());
            int radiobtn = conveyance_radio.getCheckedRadioButtonId();
            wehType = (RadioButton)findViewById(radiobtn);
            expensesRequest.setVehicleType(wehType.getText().toString());
            expensesRequest.setAmount(allowance.getText().toString().trim());
            expensesRequest.setLocationFrom(f_Location.getText().toString().trim());
            expensesRequest.setLocationTo(t_Location.getText().toString().trim());
            expensesRequest.setArea1(areas1.getText().toString().trim());
            expensesRequest.setArea2(areas2.getText().toString().trim());
            expensesRequest.setArea3(areas3.getText().toString().trim());
            expensesRequest.setDiscription(desc.getText().toString().trim());
            expensesRequest.setDistance(dist.getText().toString().trim());
            expensesRequest.setTotalAmount(amou.getText().toString().trim());
            expensesRequest.setAdvanceAmount(advance_t.getText().toString().trim());
            expensesRequest.setBalanceAmount(amount_r.getText().toString().trim());
            expensesRequest.setFoodBillno(foodBill.getText().toString().trim());
            expensesRequest.setFoodAmount(foodAmount.getText().toString().trim());
            expensesRequest.setHotelBillno(hotelBill.getText().toString().trim());
            expensesRequest.setHotelAmount(hotelAmount.getText().toString().trim());

            expensesResponses = new ExpensesResponses();
            UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);
            Call<ExpensesResponses> callEnqueue = userAPICall.postExpen(sessionManager.getToken(this), expensesRequest);
            callEnqueue.enqueue(new Callback<ExpensesResponses>() {
                @Override
                public void onResponse(Call<ExpensesResponses> call, retrofit2.Response<ExpensesResponses> response) {
                    expensesResponses = response.body();
                    if(expensesResponses != null){
                        Log.d("viswa Tezt", expensesResponses.getMessage().toString());
                        Toast.makeText(getApplicationContext(),
                                ""+expensesResponses.getMessage(), Toast.LENGTH_SHORT).show();

                        if(expensesResponses.getMessage().equals("Expense applied successfully")){
                            Intent v = new Intent(Expenses.this, MainActivity.class);
                            startActivity(v);
                        }

                    }
                    pDialog.dismiss();

                }

                @Override
                public void onFailure(Call<ExpensesResponses> call, Throwable t) {
                    pDialog.dismiss();
                    Toast.makeText(myContext,  t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        }else{
            PremiumEngine.myInstance.snackbarNoInternet(myContext);
        }

    }

    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                return new DatePickerDialog(this,
                        pDateSetListener,
                        pYear, pMonth, pDay);
        }
        return null;
    }

}
