package com.ifive.premiumsfa.ui.login;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ifive.premiumsfa.MainActivity;
import com.ifive.premiumsfa.R;
import com.ifive.premiumsfa.adapter.spinner_adapter.CompanySpinnerAdapter;
import com.ifive.premiumsfa.adapter.spinner_adapter.OutLetSpinner;
import com.ifive.premiumsfa.adapter.spinner_adapter.ProductListSpinnerAdapter;
import com.ifive.premiumsfa.datas.SessionManager;
import com.ifive.premiumsfa.datas.models.requests.GeoLocation;
import com.ifive.premiumsfa.datas.models.requests.LoginRequest;
import com.ifive.premiumsfa.datas.models.requests.ProductsRequests;
import com.ifive.premiumsfa.datas.models.responses.CompanyListResponse;
import com.ifive.premiumsfa.datas.models.responses.LoginResponse;
import com.ifive.premiumsfa.datas.models.responses.OutletList;
import com.ifive.premiumsfa.datas.remote.UserAPICall;
import com.ifive.premiumsfa.engine.PremiumEngine;
import com.ifive.premiumsfa.engine.RetroFitEngine;
import com.ifive.premiumsfa.gps.GPSTracker;
import com.ifive.premiumsfa.ui.ContractorDCR;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity{


    @BindView(R.id.emailID)
    EditText emailID;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.companys)
    Spinner company;
    @BindView(R.id.com)
    TextView coms;
    LoginResponse responseMsg;
    private SessionManager session;
    private String deviceNumber;
    private ProgressDialog pDialog;
    private double latitude=0.0,longitude=0.0;
    List<PermissionDeniedResponse> permissionsDenied;
    private CompanySpinnerAdapter companySpinnerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        getDatasList();
        session = new SessionManager();
        pDialog = PremiumEngine.myInstance.getProgDialog(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (session.getToken(this) != null) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            finish();
            startActivity(intent);
        }
    }

    private void checkGPSPermission() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            // do you work now
                        }else{
                            permissionsDenied = report.getDeniedPermissionResponses();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .onSameThread()
                .check();
    }

    private void turnGPSOn(){
        Toast.makeText(this, "Please Enable GPS", Toast.LENGTH_SHORT).show();
        try{
            Intent intent1 = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent1);
        }catch (Exception e) {

        }
    }

    @OnClick(R.id.login_button)
    public void openMain(){
        if (PremiumEngine.myInstance.isEmpty(emailID.getText().toString())) {
            emailID.setError("Username is required!");
        } else if (PremiumEngine.myInstance.isEmpty(password.getText().toString())) {
            password.setError("Wrong Password!");
        } else {
            LoginRequest loginRequest = new LoginRequest();
            loginRequest.setUsername(emailID.getText().toString());
            loginRequest.setPassword(password.getText().toString());
            loginRequest.setCompany(coms.getText().toString());
            if (PremiumEngine.isNetworkAvailable(this)) {
                loginAPI(loginRequest);
            } else {
                PremiumEngine.myInstance.snackbarNoInternet(this);
            }
        }

       /* Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        finish();
        startActivity(intent);*/
    }

    private void setCompanySpinner() {
        companySpinnerAdapter = new CompanySpinnerAdapter(this,
                android.R.layout.simple_spinner_item,
                PremiumEngine.companyListResponses);
        company.setAdapter(companySpinnerAdapter);
        company.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                // Here you get the current item (a User object) that is selected by its position
                PremiumEngine.myInstance.comRes = companySpinnerAdapter.getItem(position);
                coms.setText(companySpinnerAdapter.getItem(position).getCompanyId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {
            }
        });
    }

    public void getDatasList() {
//        pDialog.show();
        PremiumEngine.myInstance.companyListResponses = new ArrayList<>();
        UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);
        ProductsRequests productsRequests = new ProductsRequests();
        // productsRequests.setCompanyId(11);


        Call<List<CompanyListResponse>> callEnqueue = userAPICall.getCompanylist("AceNKi4Go8hZ4lLmnS8659VOzY5pgUTyMoEFDU5i");
        callEnqueue.enqueue(new Callback<List<CompanyListResponse>>() {
            @Override
            public void onResponse(Call<List<CompanyListResponse>> call, Response<List<CompanyListResponse>> response) {
                PremiumEngine.myInstance.companyListResponses = response.body();

                if (PremiumEngine.myInstance.companyListResponses != null) {
                    Toast.makeText(LoginActivity.this,
                            "Ready to Process", Toast.LENGTH_SHORT).show();
                    setCompanySpinner();
                }
                pDialog.dismiss();
            }

            @Override
            public void onFailure(Call<List<CompanyListResponse>> call, Throwable t) {
                Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                pDialog.dismiss();
            }
        });
    }

    private void loginAPI(LoginRequest loginRequest) {
        pDialog.show();
        responseMsg = new LoginResponse();
       // loginRequest.setGeoLocation(getLatLonFromGPS());
        UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);
        Call<LoginResponse> callEnqueue = userAPICall.login(loginRequest);
        callEnqueue.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                responseMsg = response.body();
                if (responseMsg != null) {
                    Toast.makeText(LoginActivity.this,
                            "" + responseMsg.getMessage(), Toast.LENGTH_SHORT).show();
                    if (responseMsg != null) {
                        session.setPreferences(LoginActivity.this, responseMsg);
                        if(responseMsg.getRememberToken()!=null){
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            finish();
                            startActivity(intent);
                        }
                    }
                }
                pDialog.dismiss();

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private GeoLocation getLatLonFromGPS() {
        GPSTracker gps = new GPSTracker(this);
        GeoLocation gpsLatLong = new GeoLocation();
        if (gps.canGetLocation()) {
            gpsLatLong.setLatitude(gps.getLatitude());
            gpsLatLong.setLongitude(gps.getLongitude());
        } else {
            gps.showSettingsAlert();
        }
        return gpsLatLong;
    }

    public void getDeviceIMEI() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 123);
        } else {
            TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
            deviceNumber = telephonyManager.getDeviceId();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 123:
                getDeviceIMEI();
                break;

            default:
                break;
        }
    }
}
