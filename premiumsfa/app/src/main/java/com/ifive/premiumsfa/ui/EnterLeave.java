package com.ifive.premiumsfa.ui;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.ifive.premiumsfa.R;
import com.ifive.premiumsfa.ui.base.BaseActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import butterknife.OnTouch;

/**
 * Created by iftpc011 on 22/3/18.
 */

public class EnterLeave extends BaseActivity {

    @BindView(R.id.leave_oId)
    TextView org_id;
    @BindView(R.id.leave_code)
    TextView lCode;
    @BindView(R.id.leave_uid)
    TextView user_id;
    @BindView(R.id.leave_empid)
    TextView emp_ids;
    @BindView(R.id.lv_model)
    TextView lv_mod;
    @BindView(R.id.fwId)
    TextView fId;
    @BindView(R.id.leave_fwd)
    Spinner fwd;
    @BindView(R.id.strt_date)
    EditText stDate;
    @BindView(R.id.end_date)
    EditText endDate;
    @BindView(R.id.start_time)
    EditText stTime;
    @BindView(R.id.end_time)
    EditText edTime;
    @BindView(R.id.noOf_days)
    EditText noOfday;
    @BindView(R.id.noOf_hours)
    EditText noOfhrs;
    @BindView(R.id.reason)
    EditText reasons;
    @BindView(R.id.leaveStatus)
    EditText status;
    @BindView(R.id.leave_typ)
    Spinner sp;
    @BindView(R.id.leave_submit)
    Button smt;
    @BindView(R.id.leave_cancel)
    Button cnsl;
    @BindView(R.id.type)
    LinearLayout lay1;
    @BindView(R.id.model)
    LinearLayout lay2;
    @BindView(R.id.hours)
    LinearLayout lay3;
    @BindView(R.id.days)
    LinearLayout lay4;
    @BindView(R.id.for_time1)
    LinearLayout times1;
    @BindView(R.id.for_time2)
    LinearLayout times2;
    @BindView(R.id.for_date1)
    LinearLayout dates1;
    @BindView(R.id.for_date2)
    LinearLayout dates2;
    Context myContext;

    private Calendar myCalendar;
    private Calendar myCalendar2;
    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog, fromTimePickerDialog, toTimePickerDialog;
    private SimpleDateFormat dateFormatter;
    Calendar calendar3;
    int year1, year2;
    int month1, month2;
    int day1, day2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enter_leave);
        ButterKnife.bind(this);

        dateFormatter = new SimpleDateFormat("yyy-MM-dd", Locale.US);

        myCalendar = Calendar.getInstance();
        myCalendar2 = Calendar.getInstance();

        calendar3 = Calendar.getInstance();
        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                view.setMinDate(System.currentTimeMillis());
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                year1 = year;
                month1 = monthOfYear;
                day1 = dayOfMonth;

                stDate.setText(new StringBuilder().append(year1).append("-").append(month1 + 1)
                        .append("-").append(day1)
                        .append(" "));
                showDiff();
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        toDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                view.setMinDate(System.currentTimeMillis());
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                year2 = year;
                month2 = monthOfYear;
                day2 = dayOfMonth;

                //endDate.setText(dateFormatter.format(newDate.getTime()));
                endDate.setText(new StringBuilder().append(year2).append("-").append(month2 + 1)
                        .append("-").append(day2)
                        .append(" "));
                showDiff();

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        Calendar newCalendars = Calendar.getInstance();

        String date1 = new StringBuilder().append(year1).append("-").append(month1 + 1)
                .append("-").append(day1)
                .append(" ").toString();

        String date2 = new StringBuilder().append(year2).append("-").append(month2 + 1)
                .append("-").append(day2)
                .append(" ").toString();
        try {


            Date dateBefore = dateFormatter.parse(date1);
            Date dateAfter = dateFormatter.parse(date2);
            long difference = dateAfter.getTime() - dateBefore.getTime();
            float daysBetween = (difference / (1000 * 60 * 60 * 24));
            Log.d("check date", "date"+daysBetween);
        }catch (Exception e) {
            e.printStackTrace();
        }
        fromTimePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                view.setMinDate(System.currentTimeMillis());
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                int mYear, mMonth, mDay;
                Calendar mcurrentDate = Calendar.getInstance();
                mYear = mcurrentDate.get(Calendar.YEAR);
                mMonth = mcurrentDate.get(Calendar.MONTH);
                mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                if (year == mYear && (monthOfYear + 1) == mMonth + 1) {
                    if (dayOfMonth < mDay) {
                        Toast.makeText(getApplicationContext(), "invalid date", Toast.LENGTH_LONG).show();
                        return;
                    }
                }

                timePicker1();

            }

        }, myCalendar.get(Calendar.YEAR), newCalendars.get(Calendar.MONTH), newCalendars.get(Calendar.DAY_OF_MONTH));



        toTimePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                view.setMinDate(System.currentTimeMillis());
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                myCalendar2.set(Calendar.YEAR, year);
                myCalendar2.set(Calendar.MONTH, monthOfYear);
                myCalendar2.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                timePicker2();
            }

        }, myCalendar.get(Calendar.YEAR), newCalendars.get(Calendar.MONTH), newCalendars.get(Calendar.DAY_OF_MONTH));
    }

    @OnTouch(R.id.strt_date)
    public boolean stDate(View view) {
        fromDatePickerDialog.getDatePicker().setMinDate(new Date().getTime());
        fromDatePickerDialog.show();
        return false;
    }

    @OnTouch(R.id.end_date)
    public boolean enDate(View view) {
        toDatePickerDialog.getDatePicker().setMinDate(new Date().getTime());
        toDatePickerDialog.show();
        return false;
    }

    @OnTouch(R.id.start_time)
    public boolean stTime(View view) {
        fromTimePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
        fromTimePickerDialog.getDatePicker().setMinDate(new Date().getTime());
        fromTimePickerDialog.show();
        return false;
    }

    @OnTouch(R.id.end_time)
    public boolean enTime(View view) {
        toTimePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
        toTimePickerDialog.getDatePicker().setMinDate(new Date().getTime());
        toTimePickerDialog.show();
        return false;
    }

    @OnItemSelected(R.id.leave_typ)
    public void enTime(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();
        //lCode.setText(getNames(position));
        if (item.equals("CASUAL LEAVE")) {
            lay2.setVisibility(View.VISIBLE);
            lay3.setVisibility(View.GONE);
            lay4.setVisibility(View.VISIBLE);
            times1.setVisibility(View.GONE);
            times2.setVisibility(View.GONE);
            dates1.setVisibility(View.VISIBLE);
            dates2.setVisibility(View.VISIBLE);
        } else if (item.equals("SICK LEAVE")) {
            lay2.setVisibility(View.VISIBLE);
            lay3.setVisibility(View.GONE);
            lay4.setVisibility(View.VISIBLE);
            times1.setVisibility(View.GONE);
            times2.setVisibility(View.GONE);
            dates1.setVisibility(View.VISIBLE);
            dates2.setVisibility(View.VISIBLE);
        } else if (item.equals("ON DUTY")) {
            //lay3.setVisibility(View.VISIBLE);
            lay2.setVisibility(View.GONE);
            lay3.setVisibility(View.VISIBLE);
            lay4.setVisibility(View.GONE);

            times1.setVisibility(View.VISIBLE);
            times2.setVisibility(View.VISIBLE);
            dates1.setVisibility(View.GONE);
            dates2.setVisibility(View.GONE);
        }
    }

    @OnItemSelected(R.id.leave_model)
    public void lvModal(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();

        if (item.equals("Half Day")) {

            lay3.setVisibility(View.VISIBLE);
            lay4.setVisibility(View.GONE);

            times1.setVisibility(View.VISIBLE);
            times2.setVisibility(View.VISIBLE);
            dates1.setVisibility(View.GONE);
            dates2.setVisibility(View.GONE);
            lv_mod.setText("1");
        } else if (item.equals("Full Day")) {

            lay3.setVisibility(View.GONE);
            lay4.setVisibility(View.VISIBLE);
            times1.setVisibility(View.GONE);
            times2.setVisibility(View.GONE);
            dates1.setVisibility(View.VISIBLE);
            dates2.setVisibility(View.VISIBLE);
            lv_mod.setText("2");
        }
    }

    public void showDiff() {
        int Days = 0;
        if (month1 == month2 && day2 == day1) {

            Days = day1-day2 +1;

        } else if (month2 == month1) {
            if (day2 > day1) {
                Days = day2 - day1 + 1;
            }
        } else if (month2 > month1) {
            if (day2 > day1) {
                int m = month2 - month1;
                Days = (m * 30 + (day2 - day1)) + 1;
            }
        } /*else if (month1 == month2) {
            if (day2 == day1) {
                Days = 1;
                noOfday.setText(String.valueOf(1));
            }*/ else
            Toast.makeText(getApplicationContext(), "Invalid Out Date", Toast.LENGTH_LONG).show();
        noOfday.setText(String.valueOf(Days));

    }

    private void timePicker1() {
        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        myCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        myCalendar.set(Calendar.MINUTE, minute);
                        stTime.setText(
                                myCalendar.get(Calendar.DAY_OF_MONTH) + "-" +
                                        (myCalendar.get(Calendar.MONTH) + 1) + "-" +
                                        myCalendar.get(Calendar.YEAR) + " " +
                                        myCalendar.get(Calendar.HOUR_OF_DAY) + ":" +
                                        myCalendar.get(Calendar.MINUTE) + ":" + "27");
                        showTimeDiffs();
                    }
                }, myCalendar.get(Calendar.HOUR_OF_DAY), myCalendar.get(Calendar.MINUTE), false);
        timePickerDialog.show();

    }

    public void showTimeDiffs() {
        long difference = myCalendar2.getTimeInMillis() - myCalendar.getTimeInMillis();

        long x = difference / 1000;
        long seconds = x % 60;
        x /= 60;
        long minutes = x % 60;
        x /= 60;
        long hours = x % 24;
        x /= 24;
        long days = x;
        noOfhrs.setText(longToString(hours) + ":" + longToString(minutes) + ":" + longToString(seconds));
    }

    public static String longToString(long l) {
        return String.valueOf(l);
    }

    private void timePicker2() {
        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        myCalendar2.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        myCalendar2.set(Calendar.MINUTE, minute);
                        edTime.setText(
                                myCalendar2.get(Calendar.DAY_OF_MONTH) + "-" +
                                        (myCalendar2.get(Calendar.MONTH) + 1) + "-" +
                                        myCalendar2.get(Calendar.YEAR) + " " +
                                        myCalendar2.get(Calendar.HOUR_OF_DAY) + ":" +
                                        myCalendar2.get(Calendar.MINUTE) + ":" + "27");
                        showTimeDiffs();
                    }
                }, myCalendar2.get(Calendar.HOUR_OF_DAY), myCalendar2.get(Calendar.MINUTE), false);
        timePickerDialog.show();

    }
}
