package com.ifive.premiumsfa;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.messaging.FirebaseMessaging;
import com.ifive.premiumsfa.datas.SessionManager;
import com.ifive.premiumsfa.datas.database.DatabaseHandler;
import com.ifive.premiumsfa.datas.models.requests.AttendanceRequest;
import com.ifive.premiumsfa.datas.models.requests.DcrRequest;
import com.ifive.premiumsfa.datas.models.responses.AttendanceResponse;
import com.ifive.premiumsfa.datas.models.responses.DcrResponses;
import com.ifive.premiumsfa.datas.remote.UserAPICall;
import com.ifive.premiumsfa.engine.PremiumEngine;
import com.ifive.premiumsfa.engine.RetroFitEngine;
import com.ifive.premiumsfa.fcm.Config;
import com.ifive.premiumsfa.gps.GeoLocation;
import com.ifive.premiumsfa.gps.LocationMonitoringService;
import com.ifive.premiumsfa.ui.ContractorDCR;
import com.ifive.premiumsfa.ui.base.BaseActivity;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by Syss4 on 2/2/2018.
 */

public class Attendance extends BaseActivity {

    DatabaseHandler db;
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    private boolean mAlreadyStartedService = false;
    @BindView(R.id.checkin_btn)
    Button checkin;
    @BindView(R.id.checkout_btn)
    Button checkout;
    @BindView(R.id.getTime)
    TextView times;
    @BindView(R.id.getDate)
    TextView dates;

    @BindView(R.id.start_layout)
    LinearLayout startLayout;
    @BindView(R.id.end_layout)
    LinearLayout endLayout;
    @BindView(R.id.end_time)
    TextView endTime;
    @BindView(R.id.start_time)
    TextView startTime;
    @BindView(R.id.tot_layout)
    LinearLayout totLayout;
    @BindView(R.id.tot_hour)
    TextView totHour;
    SessionManager sessionManager;

    Typeface typeface;
    ActionBar actionBar;
    Calendar myCalendar;
    Calendar myCalendars;

    AttendanceRequest attendanceRequest;
    AttendanceResponse attendanceResponse;

    private ProgressDialog pDialog;


    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private BroadcastReceiver gpsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().matches(LocationManager.PROVIDERS_CHANGED_ACTION)) {
                //Do your stuff on GPS status change

                LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    Toast.makeText(context, "Please Enable GPS", Toast.LENGTH_SHORT).show();
                    try {
                        Intent intent1 = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent1);
                    } catch (Exception e) {

                    }
                } else {
                    Log.e(TAG, "Disabled");
                }
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.attendance);
        ButterKnife.bind(this);
        pDialog = PremiumEngine.myInstance.getProgDialog(this);
        dates.setText(PremiumEngine.getCurrentDate());
        times.setText(PremiumEngine.getCurrentTime());

        //typeface = PremiumEngine.myInstance.getCommonTypeFace(this);
        SpannableString titleSpan = new SpannableString("Sales Order");
       /* titleSpan.setSpan(new CustomTypefaceSpan("" , typeface), 0 ,
                titleSpan.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);*/
        actionBar = getSupportActionBar();
        actionBar.setTitle(titleSpan);
        myCalendar = Calendar.getInstance();
        myCalendars = Calendar.getInstance();
        sessionManager = new SessionManager();
        setAttendanceLayout();


        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    String message = intent.getStringExtra("message");
//                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
                }
            }
        };

        LocalBroadcastManager.getInstance(this).registerReceiver(
                new BroadcastReceiver() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        String latitude = intent.getStringExtra(LocationMonitoringService.EXTRA_LATITUDE);
                        String longitude = intent.getStringExtra(LocationMonitoringService.EXTRA_LONGITUDE);
                        if (latitude != null && longitude != null) {
                        }
                    }
                }, new IntentFilter(LocationMonitoringService.ACTION_LOCATION_BROADCAST)
        );
        registerReceiver(gpsReceiver, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION));

    }

    private void setAttendanceLayout() {
        if (sessionManager.getStartTime(this) == null) {
            startLayout.setVisibility(View.GONE);
            endLayout.setVisibility(View.GONE);
            totLayout.setVisibility(View.GONE);
            checkin.setVisibility(View.VISIBLE);
            checkout.setVisibility(View.VISIBLE);
        } else {
            startLayout.setVisibility(View.VISIBLE);
            checkin.setVisibility(View.VISIBLE);
            startTime.setText(sessionManager.getStartTime(this));
            if (sessionManager.getEndTime(this) == null) {
                endLayout.setVisibility(View.GONE);
                totLayout.setVisibility(View.GONE);
                checkout.setVisibility(View.VISIBLE);
            } else {
                endLayout.setVisibility(View.VISIBLE);
                startLayout.setVisibility(View.VISIBLE);
                checkout.setVisibility(View.VISIBLE);
                endTime.setText(sessionManager.getEndTime(this));
            }
        }
    }

    @OnClick(R.id.checkin_btn)
    public void checkinClick(View view) {
        turnGPSOn();

        sessionManager.setStartEmployeeWorkTime(PremiumEngine.myInstance.getCalenderTime(myCalendar), this);
        startTime.setText(PremiumEngine.myInstance.getCalenderTime(myCalendar));
        setAttendanceLayout();
    }

    @OnClick(R.id.checkout_btn)
    public void checkoutClick(View view) {
        stopService(new Intent(this, LocationMonitoringService.class));
        checkin.setEnabled(true);
        checkout.setEnabled(false);
        Toast.makeText(this, "Checked out Successfully!", Toast.LENGTH_SHORT).show();
        sessionManager.setEndEmployeeWorkTime(PremiumEngine.myInstance.getCalenderTime(myCalendars), this);
        endTime.setText(PremiumEngine.myInstance.getCalenderTime(myCalendars));

        long diff = myCalendar.getTimeInMillis() - myCalendars.getTimeInMillis();
        int hours = (int) (diff / (1000 * 60 * 60));
        int minutes = (int) (diff / (1000 * 60));
        int seconds = (int) (diff / (1000));
        totHour.setText(hours + "-" + minutes + "-" + seconds);
        setAttendanceLayout();

        //attendance_post_start

        if (PremiumEngine.isNetworkAvailable(this)) {
            pDialog.show();
            attendanceRequest = new AttendanceRequest();
            attendanceRequest.setStartTime(startTime.getText().toString().trim());
            attendanceRequest.setEndTime(endTime.getText().toString());
            attendanceRequest.setTotalHours(PremiumEngine.myInstance.convertStringToInt(totHour.getText().toString()));
            attendanceRequest.setUserId(sessionManager.getUserData(this).getUserId().toString());


            attendanceResponse = new AttendanceResponse();
            UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);
            Call<AttendanceResponse> callEnqueue = userAPICall.postAtten("yB4vhewxaggTsQmmlUr41IaC8SWMj8iZdEQNiHnm", attendanceRequest);
            callEnqueue.enqueue(new Callback<AttendanceResponse>() {
                @Override
                public void onResponse(Call<AttendanceResponse> call, retrofit2.Response<AttendanceResponse> response) {
                    attendanceResponse = response.body();
                    if (attendanceResponse != null) {
                        Toast.makeText(getApplicationContext(),
                                "" + attendanceResponse.getMessage(), Toast.LENGTH_SHORT).show();


                    }
                    pDialog.dismiss();

                }

                @Override
                public void onFailure(Call<AttendanceResponse> call, Throwable t) {
                    pDialog.dismiss();
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        }else {
            PremiumEngine.myInstance.snackbarNoInternet(this);
        }
    }


    @Override
    protected void onResume () {
        super.onResume();

        if(isMyServiceRunning(LocationMonitoringService.class)){
            checkin.setEnabled(false);
            checkout.setEnabled(true);
        }else{
            checkin.setEnabled(true);
            checkout.setEnabled(false);
        }
        if (PremiumEngine.isNetworkAvailable(this)) {
            getDatasList();
        } else {
            PremiumEngine.myInstance.snackbarNoInternet(this);
        }
        //setNavigationMenu();
//            Toast.makeText(this, "Welcome "+userData.getEmployeeName() +" to Rathna Cements!", Toast.LENGTH_SHORT).show();


        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
//        NotificationUtils.clearNotifications(getApplicationContext());
    }

    public void getDatasList() {
//        pDialog.show();
        // SalesEnginer.myInstance.listDatasResponse = new ListDatasResponse();
        UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);
        //ListDatasRequest listDatasRequest = new ListDatasRequest();

        db = new DatabaseHandler(this);
        List<GeoLocation> geoLocations = db.getAllGPSLocations();


    }


    public void startTracking() {
        //Check whether this user has installed Google play service which is being used by Location updates.
        if (isGooglePlayServicesAvailable()) {
            //Passing null to indicate that it is executing for the first time.
            startTracking2(null);

        } else {
            Toast.makeText(this, R.string.no_google_playservice_available, Toast.LENGTH_LONG).show();
        }
    }

    public Boolean startTracking2(DialogInterface dialog) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            promptInternetConnect();
            return false;
        }
        if (dialog != null) {
            dialog.dismiss();
        }

        //Yes there is active internet connection. Next check Location is granted by user or not.
        if (checkPermissions()) { //Yes permissions are granted by the user. Go to the next step.
            startTracking3();
        } else {  //No user has not granted the permissions yet. Request now.
            requestPermissions();
        }
        return true;
    }

    public void promptInternetConnect() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.title_alert_no_intenet);
        builder.setMessage(R.string.msg_alert_no_internet);
        String positiveText = getString(R.string.btn_label_refresh);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Block the Application Execution until user grants the permissions
                        if (startTracking2(dialog)) {
                            //Now make sure about location permission.
                            if (checkPermissions()) {
                                //Step 2: Start the Location Monitor Service
                                //Everything is there to start the service.
                                startTracking3();
                            } else if (!checkPermissions()) {
                                requestPermissions();
                            }
                        }
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void startTracking3() {
        //And it will be keep running until you close the entire application from task manager.
        //This method will executed only once.
        //  if (!mAlreadyStartedService) {
        //Start location sharing service to app server.........
        Intent intent = new Intent(Attendance.this, LocationMonitoringService.class);
        startService(intent);
        mAlreadyStartedService = true;
        checkin.setEnabled(false);
        checkout.setEnabled(true);
        Toast.makeText(this, "Checked in Successfully!", Toast.LENGTH_SHORT).show();
        //Ends................................................
       /* }else*/
        {
            //  mAlreadyStartedService = false;
        }
    }

    public boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(this);
        if (status != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(this, status, 2404).show();
            }
            return false;
        }
        return true;
    }

    public boolean checkPermissions() {
        int permissionState1 = ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION);
        int permissionState2 = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        return permissionState1 == PackageManager.PERMISSION_GRANTED && permissionState2 == PackageManager.PERMISSION_GRANTED;

    }

    public void requestPermissions() {

        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION);

        boolean shouldProvideRationale2 =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        android.Manifest.permission.ACCESS_COARSE_LOCATION);


        // Provide an additional rationale to the img_user. This would happen if the img_user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale || shouldProvideRationale2) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            showSnackbar(R.string.permission_rationale,
                    android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(Attendance.this,
                                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                                    REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    });
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the img_user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    public void showSnackbar(final int mainTextStringId, final int actionStringId,
                             View.OnClickListener listener) {
        Snackbar.make(
                findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If img_user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.i(TAG, "Permission granted, updates requested, starting location updates");
                startTracking3();
            } else {
                // Permission denied.

                // Notify the img_user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the img_user for permission (device policy or "Never ask
                // again" prompts). Therefore, a img_user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                showSnackbar(R.string.permission_denied_explanation,
                        R.string.settings, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
            }
        }
    }

    private void turnGPSOn() {
        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Toast.makeText(this, "Please Enable GPS", Toast.LENGTH_SHORT).show();
            try {
                Intent intent1 = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent1);
            } catch (Exception e) {

            }
        } else {
            startTracking();

        }
    }

    public boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}

