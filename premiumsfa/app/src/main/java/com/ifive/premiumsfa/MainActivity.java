package com.ifive.premiumsfa;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.ifive.premiumsfa.datas.SessionManager;
import com.ifive.premiumsfa.ui.AddCustomer;
import com.ifive.premiumsfa.ui.ContractorDCR;
import com.ifive.premiumsfa.ui.Expenses;
import com.ifive.premiumsfa.ui.LeaveEntry;
import com.ifive.premiumsfa.ui.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends BaseActivity {

    @BindView(R.id.home_attendance)
    CardView attendance;
    @BindView(R.id.home_movement)
    CardView movement;
    @BindView(R.id.home_collections)
    CardView collection;
    @BindView(R.id.home_remarks)
    CardView remark;
    @BindView(R.id.home_expenses)
    CardView expense;
    @BindView(R.id.home_target)
    CardView target;
    @BindView(R.id.home_leave)
    CardView leave;
    @BindView(R.id.home_demo)
    CardView cust;
    @BindView(R.id.home_name)
    TextView name;
    @BindView(R.id.home_email)
    TextView email;
    SessionManager sessionManager;

  //  private SessionManager session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        sessionManager = new SessionManager();
        name.setText(sessionManager.getUserData(this).getFirstName()+""+sessionManager.getUserData(this).getLastName());
        email.setText(sessionManager.getUserData(this).getEmail());
    }

    @OnClick(R.id.home_attendance)
    public void clickAttendance(View view) {
        Intent n = new Intent(this, Attendance.class);
        startActivity(n);
    }

    @OnClick(R.id.home_remarks)
    public void clickRemarks(View view) {
        Intent n = new Intent(this, ContractorDCR.class);
        startActivity(n);
    }

    @OnClick(R.id.home_movement)
    public void clickMovement(View view){
        Intent s = new Intent(this, Movement.class);
        startActivity(s);
    }

    @OnClick(R.id.home_expenses)
    public void clickExpenses(View view){
        Intent ex = new Intent(this, Expenses.class);
        startActivity(ex);
    }

    @OnClick(R.id.home_target)
    public void clickTarget(View view){
        Intent tar = new Intent(this, Target.class);
        startActivity(tar);
    }

    @OnClick(R.id.home_leave)
    public void clickLeave(View view){
        Intent tar = new Intent(this, LeaveEntry.class);
        startActivity(tar);
    }

    @OnClick(R.id.home_demo)
    public void clickCust(View view){
        Intent tar = new Intent(this, AddCustomer.class);
        startActivity(tar);
    }

   /* @Override
    protected void onResume() {
        super.onResume();
        if (session.getToken(this) != null) {
            NotificationScheduler.cancelReminder(this,AlarmReceiver.class);
            addNotificationTiming(12,40);
        } else {
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
        }
    }

    private void addNotificationTiming(int hour, int minute) {
        NotificationScheduler.setReminder(MainActivity.this,
                AlarmReceiver.class, hour, minute);
    }*/

}
