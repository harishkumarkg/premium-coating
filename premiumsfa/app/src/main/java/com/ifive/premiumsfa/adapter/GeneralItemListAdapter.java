/*
package com.ifive.premiumsfa.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.ifive.premiumsfa.Movement;
import com.ifive.premiumsfa.R;
import com.ifive.premiumsfa.adapter.spinner_adapter.ProductListSpinnerAdapter;
import com.ifive.premiumsfa.engine.PremiumEngine;

import java.util.List;

*/
/**
 * Created by Syss4 on 2/12/2018.
 *//*


public class GeneralItemListAdapter extends RecyclerView.Adapter<GeneralItemListAdapter.MyViewHolder> {
    private Context context;
    private List<ItemLists> cartList;
    Movement movement;
    List<ProductList> productsList;
    List<SchemeList> schemeLists;
    private List<ItemLists> itemLists = null;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public Spinner productSpinner,schemeSpinner;
        public LinearLayout viewForeground;
        ProductListSpinnerAdapter productListSpinnerAdapter;
        SchemeListSpinnerAdapter schemeListsAdapter;
        EditText quantity;
        TextView subPrice,addlAmount;
        public MyViewHolder(View view) {
            super(view);
            viewForeground = view.findViewById(R.id.view_foreground);
            productSpinner = view.findViewById(R.id.product_spinner);
            schemeSpinner = view.findViewById(R.id.schemeSpinner);
            quantity = view.findViewById(R.id.quantity);
            addlAmount = view.findViewById(R.id.addl_amount);
            subPrice = view.findViewById(R.id.sub_price);
            productListSpinnerAdapter= new ProductListSpinnerAdapter(context,
                    android.R.layout.simple_spinner_item,
                    productsList);
            productSpinner.setAdapter(productListSpinnerAdapter);
            schemeListsAdapter= new SchemeListSpinnerAdapter(context,
                    android.R.layout.simple_spinner_item,
                    schemeLists);
            schemeSpinner.setAdapter(schemeListsAdapter);
        }
    }

    public GeneralItemListAdapter(Context context, List<ItemLists> cartList,
                                  Movement saleOrderActivity, List<ProductList> productsList, List<SchemeList> schemeLists) {
        this.context = context;
        this.cartList = cartList;
        this.movement = Movement;
        this.productsList = productsList;
        this.schemeLists = schemeLists;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.general_item_list, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ItemLists item = cartList.get(position);
        holder.productSpinner.setSelection(item.getProductPosition());
        holder.schemeSpinner.setSelection(item.getSchemePosition());
        if(item.getAddlAmount()==0.0){
            holder.addlAmount.setText("");
        }else{
            holder.addlAmount.setText(PremiumEngine.myInstance.formatNumber(item.getAddlAmount())+"");
        }
        double totalSubPrice = PremiumEngine.myInstance.calculateProductSubAmount(item);
        holder.subPrice.setText(PremiumEngine.myInstance.formatNumber(totalSubPrice)+"");
        holder.productSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int pPosition, long id) {
                // Here you get the current item (a User object) that is selected by its position
                movement.setItemPosition(holder.productListSpinnerAdapter.getItem(pPosition),pPosition,position);
                item.setProductPosition(pPosition);
                item.setProductId(holder.productListSpinnerAdapter.getItem(pPosition).getProductId());
                item.setHsnCode(holder.productListSpinnerAdapter.getItem(pPosition).getHsnCode());
                item.setPrice(holder.productListSpinnerAdapter.getItem(pPosition).getPrice());
                item.setProductId(holder.productListSpinnerAdapter.getItem(pPosition).getProductId());
                item.setProductCode(holder.productListSpinnerAdapter.getItem(pPosition).getProductCode());
                item.setDescription(holder.productListSpinnerAdapter.getItem(pPosition).getProductDescription());
                item.setGst(holder.productListSpinnerAdapter.getItem(pPosition).getGst());
                item.setProductName(holder.productListSpinnerAdapter.getItem(pPosition).getProductName());
                double totalSubPrice = PremiumEngine.myInstance.calculateProductSubAmount(item);
                holder.subPrice.setText(PremiumEngine.myInstance.formatNumber(totalSubPrice)+"");
                item.setTotal(totalSubPrice);
                holder.quantity.requestFocus();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapter) {  }
        });
        holder.schemeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int sPosition, long id) {
                // Here you get the current item (a User object) that is selected by its position
                movement.setSchemeItemPosition(holder.schemeListsAdapter.getItem(sPosition),sPosition,position);
                item.setSchemePosition(sPosition);
                item.setSchemeID(holder.schemeListsAdapter.getItem(sPosition).getSchemeId());
                item.setSchemeAmount(holder.schemeListsAdapter.getItem(sPosition).getSchemeValue());
                double totalSubPrice = PremiumEngine.myInstance.calculateProductSubAmount(item);
                holder.subPrice.setText(PremiumEngine.myInstance.formatNumber(totalSubPrice)+"");
                item.setTotal(totalSubPrice);
                holder.addlAmount.requestFocus();
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {  }
        });
        if(item.getQuantity()==0){
            holder.quantity.setText("");
        }else{
            holder.quantity.setText(item.getQuantity()+"");
        }
        holder.quantity.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                int quan=PremiumEngine.myInstance.convertStringToInt(holder.quantity.getText().toString().trim());
                item.setQuantity(quan);
                saleOrderActivity.setItemQuantity(quan,position);
                double totalSubPrice = PremiumEngine.myInstance.calculateProductSubAmount(item);
                holder.subPrice.setText(PremiumEngine.myInstance.formatNumber(totalSubPrice)+"");
                item.setTotal(totalSubPrice);
//                holder.schemeSpinner.performClick();
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            public void onTextChanged(CharSequence s, int start, int beSfore, int count) {}
        });
        holder.addlAmount.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                double addAmount=PremiumEngine.myInstance.convertStringToDouble(holder.addlAmount.getText().toString().trim());
                item.setAddlAmount(addAmount);
                movement.setItemAddlAmt(addAmount,position);
                double totalSubPrice = PremiumEngine.myInstance.calculateProductSubAmount(item);
                holder.subPrice.setText(PremiumEngine.myInstance.formatNumber(totalSubPrice)+"");
                item.setTotal(totalSubPrice);
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });
    }

    @Override
    public int getItemCount() {
        return cartList.size();
    }

    public void removeItem(int position) {
        cartList.remove(position);
        movement.calculateTotalAmount();
        notifyItemRemoved(position);
    }

    public void restoreItem(ItemLists item, int position) {
        cartList.add(position, item);
        // notify item added by position
        notifyItemInserted(position);
    }
}*/
