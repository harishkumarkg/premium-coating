package com.ifive.premiumsfa.datas.models.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Syss4 on 2/15/2018.
 */

public class SalesItemLists {

    @SerializedName("town")
    @Expose
    private String town;
    @SerializedName("stockist")
    @Expose
    private String stockist;
    @SerializedName("beat")
    @Expose
    private String beat;
    @SerializedName("outlet")
    @Expose
    private String outlet;
    @SerializedName("customer_type")
    @Expose
    private String customerType;
    @SerializedName("items_lists")
    @Expose
    private List<SoItemList> itemsLists = null;
    @SerializedName("total_amount")
    @Expose
    private String totalAmount;

    @SerializedName("user_id")
    @Expose
    private String userId;

    @SerializedName("customer_id")
    @Expose
    private Integer customerId;
    @SerializedName("location_id")
    @Expose
    private String locationId;
    @SerializedName("org_id")
    @Expose
    private String orgId;

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getStockist() {
        return stockist;
    }

    public void setStockist(String stockist) {
        this.stockist = stockist;
    }

    public String getBeat() {
        return beat;
    }

    public void setBeat(String beat) {
        this.beat = beat;
    }

    public String getOutlet() {
        return outlet;
    }

    public void setOutlet(String outlet) {
        this.outlet = outlet;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public List<SoItemList> getItemsLists() {
        return itemsLists;
    }

    public void setItemsLists(List<SoItemList> itemsLists) {
        this.itemsLists = itemsLists;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }


}
