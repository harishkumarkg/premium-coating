package com.ifive.premiumsfa.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ifive.premiumsfa.OrderList;
import com.ifive.premiumsfa.R;
import com.ifive.premiumsfa.datas.models.responses.AttendListModel;
import com.ifive.premiumsfa.datas.models.responses.SalesList;
import com.ifive.premiumsfa.ui.AttendanceList;

import java.util.List;

/**
 * Created by iftpc011 on 22/3/18.
 */

public class AttendanceAdapter extends RecyclerView.Adapter<AttendanceAdapter.MyViewHolder> {

    private Context context;
    private List<AttendListModel> attendListModels;
    private AttendanceList orderList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView sTime,eTime, tHour, sNo;

        public MyViewHolder(View view) {
            super(view);
            sNo = view.findViewById(R.id.sales_serial_number);
            sTime = view.findViewById(R.id.sTime);
            eTime = view.findViewById(R.id.eTime);
            tHour = view.findViewById(R.id.tot_hour);
        }
    }

    public AttendanceAdapter(Context context, List<AttendListModel> cartList, AttendanceList orderList) {
        this.context = context;
        this.attendListModels = cartList;
        this.orderList = orderList;
    }

    @Override
    public AttendanceAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.orderlist_item, parent, false);
        return new AttendanceAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AttendanceAdapter.MyViewHolder holder, int position) {

        final AttendListModel item = attendListModels.get(position);
        holder.sNo.setText(position+1+"");
        holder.sTime.setText(item.getStartTime());
        holder.eTime.setText(item.getEndTime());
        holder.tHour.setText(item.getTotalHours());

    }

    @Override
    public int getItemCount() {
        return attendListModels.size();
    }
}

