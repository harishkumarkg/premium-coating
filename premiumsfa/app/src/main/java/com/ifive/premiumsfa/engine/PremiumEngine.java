package com.ifive.premiumsfa.engine;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

import com.ifive.premiumsfa.datas.models.requests.SoItemList;
import com.ifive.premiumsfa.datas.models.responses.CompanyListResponse;
import com.ifive.premiumsfa.datas.models.responses.OutletList;
import com.ifive.premiumsfa.datas.models.responses.ProductList;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Comp11 on 12/19/2017.
 */

public class PremiumEngine {

    public static PremiumEngine myInstance = new PremiumEngine();
    public static List<ProductList> productList;
    public static List<OutletList> outletLists;
    public static List<CompanyListResponse> companyListResponses;
    public static ArrayList<SoItemList> soItemLists = new ArrayList<SoItemList>();
    public ProductList itemSelected;
    public OutletList itemSelecteds;
    public CompanyListResponse comRes;

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    public boolean isEmpty(String string) {
        if (string.trim().equals("")) {
            return true;
        } else
            return false;
    }

    public double convertStringToDouble(String text){
        double number=0;
        try {
            number = Double.parseDouble(text);
        } catch(NumberFormatException e) {
        }
        return number;
    }

    public int convertDoubleToInt(double text){
        int number=0;
        try {
            number = (int) Math.floor(text);
        } catch(NumberFormatException e) {
        }
        return number;
    }

    public int convertStringToInt(String text){
        int number=0;
        try {
            number = Integer.parseInt(text);
        } catch(NumberFormatException e) {
        }
        return number;
    }

    public void snackbarNoInternet(Context context) {
        Snackbar snackbar = Snackbar
                .make(((Activity)context).findViewById(android.R.id.content),
                        "No Network Connection!", Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    public static ProgressDialog getProgDialog(Context context) {
        ProgressDialog progDialog = new ProgressDialog(context);
        progDialog.setMessage("Loading...");
        progDialog.setIndeterminate(false);
        progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progDialog.setCancelable(false);
        return progDialog;
    }

    public static String getCurrentDate(){
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public static String getCurrentTime(){
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("hh:mm:ss aa");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public double getGST(double totalAmount, double gstSelected) {
        try{
            return (totalAmount*gstSelected)/100;
        }catch(Exception e){

        }
        return 0;
    }

    public Animation getBlinkAnimation(){
        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(1000); //You can manage the blinking time with this parameter
        anim.setStartOffset(20);
        anim.setRepeatMode(Animation.REVERSE);
        anim.setRepeatCount(Animation.INFINITE);
        return anim;
    }

    public String getCalenderTime(Calendar myCalendar) {
        String date = myCalendar
                .get(Calendar.YEAR)+"-"+myCalendar
                .get(Calendar.MONTH)+"-"+myCalendar
                .get(Calendar.DAY_OF_MONTH)+" "+myCalendar
                .get(Calendar.HOUR_OF_DAY)+":"+myCalendar
                .get(Calendar.MINUTE)+":"+myCalendar
                .get(Calendar.SECOND);
        return date;
    }

    public String formatDate(String reqDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
        try {
             return sdf1.format(sdf.parse(reqDate));
        } catch (ParseException e) {
            e.printStackTrace();
            System.out.println("bad pattern");
        }
        return "";
    }

    public String formatDateTime(String reqDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        try {
            return sdf1.format(sdf.parse(reqDate));
        } catch (ParseException e) {
            e.printStackTrace();
            System.out.println("bad pattern");
        }
        return "";
    }

    public String getSimpleCalenderDate(Calendar myCalendar) {
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        return format.format(myCalendar.getTime());
    }

    public String getFormatedTime(int h, int m, Context context) {
        final String OLD_FORMAT = "HH:mm";
        final String NEW_FORMAT = "hh:mm a";
        String oldDateString = h + ":" + m;
        String newDateString = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT, getCurrentLocale(context));
            Date d = sdf.parse(oldDateString);
            sdf.applyPattern(NEW_FORMAT);
            newDateString = sdf.format(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newDateString;
    }

    @TargetApi(Build.VERSION_CODES.N)
    public Locale getCurrentLocale(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return context.getResources().getConfiguration().getLocales().get(0);
        } else {
            //noinspection deprecation
            return context.getResources().getConfiguration().locale;
        }
    }
}
