package com.ifive.premiumsfa.datas.models.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Syss4 on 2/19/2018.
 */

public class TownID {

    @SerializedName("town_id")
    @Expose
    private int townID;

    public int getTownID() {
        return townID;
    }

    public void setTownID(int townID) {
        this.townID = townID;
    }
}
