package com.ifive.premiumsfa.datas.models.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Syss4 on 2/17/2018.
 */

public class ExpensesRequest {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("conv_type")
    @Expose
    private String convType;
    @SerializedName("vehicle_type")
    @Expose
    private String vehicleType;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("location_from")
    @Expose
    private String locationFrom;
    @SerializedName("location_to")
    @Expose
    private String locationTo;
    @SerializedName("area1")
    @Expose
    private String area1;
    @SerializedName("area2")
    @Expose
    private String area2;
    @SerializedName("area3")
    @Expose
    private String area3;
    @SerializedName("discription")
    @Expose
    private String discription;
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("total_amount")
    @Expose
    private String totalAmount;
    @SerializedName("advance_amount")
    @Expose
    private String advanceAmount;
    @SerializedName("balance_amount")
    @Expose
    private String balanceAmount;
    @SerializedName("food_billno")
    @Expose
    private String foodBillno;
    @SerializedName("food_amount")
    @Expose
    private String foodAmount;
    @SerializedName("hotel_billno")
    @Expose
    private String hotelBillno;
    @SerializedName("hotel_amount")
    @Expose
    private String hotelAmount;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getConvType() {
        return convType;
    }

    public void setConvType(String convType) {
        this.convType = convType;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getLocationFrom() {
        return locationFrom;
    }

    public void setLocationFrom(String locationFrom) {
        this.locationFrom = locationFrom;
    }

    public String getLocationTo() {
        return locationTo;
    }

    public void setLocationTo(String locationTo) {
        this.locationTo = locationTo;
    }

    public String getArea1() {
        return area1;
    }

    public void setArea1(String area1) {
        this.area1 = area1;
    }

    public String getArea2() {
        return area2;
    }

    public void setArea2(String area2) {
        this.area2 = area2;
    }

    public String getArea3() {
        return area3;
    }

    public void setArea3(String area3) {
        this.area3 = area3;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getAdvanceAmount() {
        return advanceAmount;
    }

    public void setAdvanceAmount(String advanceAmount) {
        this.advanceAmount = advanceAmount;
    }

    public String getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(String balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public String getFoodBillno() {
        return foodBillno;
    }

    public void setFoodBillno(String foodBillno) {
        this.foodBillno = foodBillno;
    }

    public String getFoodAmount() {
        return foodAmount;
    }

    public void setFoodAmount(String foodAmount) {
        this.foodAmount = foodAmount;
    }

    public String getHotelBillno() {
        return hotelBillno;
    }

    public void setHotelBillno(String hotelBillno) {
        this.hotelBillno = hotelBillno;
    }

    public String getHotelAmount() {
        return hotelAmount;
    }

    public void setHotelAmount(String hotelAmount) {
        this.hotelAmount = hotelAmount;
    }

}
