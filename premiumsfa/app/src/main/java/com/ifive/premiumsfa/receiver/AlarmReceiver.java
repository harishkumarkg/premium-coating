package com.ifive.premiumsfa.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.ifive.premiumsfa.MainActivity;
import com.ifive.premiumsfa.datas.models.requests.GeoLocation;
import com.ifive.premiumsfa.gps.GPSTracker;

import static android.content.ContentValues.TAG;


public class AlarmReceiver extends BroadcastReceiver {
    Context context;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        LocalData localData = new LocalData(context);
        if (intent.getAction() != null && context != null) {
            if (intent.getAction().equalsIgnoreCase(Intent.ACTION_BOOT_COMPLETED)) {
                // Set the alarm here.
                Log.d(TAG, "onReceive: BOOT_COMPLETED");
                NotificationScheduler.setReminder(context, AlarmReceiver.class,
                        localData.get_hour(), localData.get_min());
                getLatLonFromGPS();
                return;
            }
        }
        //Trigger the notification
        NotificationScheduler.showNotification(context, MainActivity.class,
                "Came from Muktha SFA", "Started at Hour:"+localData.get_hour()+", Minutes :"+localData.get_min());
    }

    private GeoLocation getLatLonFromGPS() {
        GPSTracker gps = new GPSTracker(context);
        GeoLocation gpsLatLong = new GeoLocation();
        if (gps.canGetLocation()) {
            gpsLatLong.setLatitude(gps.getLatitude());
            gpsLatLong.setLongitude(gps.getLongitude());
        } else {
            gps.showSettingsAlert();
        }
        return gpsLatLong;
    }

}