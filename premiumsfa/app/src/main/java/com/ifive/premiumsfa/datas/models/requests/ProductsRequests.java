package com.ifive.premiumsfa.datas.models.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Syss4 on 2/14/2018.
 */

public class ProductsRequests {
    @SerializedName("company_id")
    @Expose
    private Integer companyId;

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }
}
