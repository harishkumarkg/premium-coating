package com.ifive.premiumsfa.adapter.spinner_adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ifive.premiumsfa.Movement;
import com.ifive.premiumsfa.R;
import com.ifive.premiumsfa.adapter.TownListAdapter;
import com.ifive.premiumsfa.datas.SessionManager;
import com.ifive.premiumsfa.datas.models.responses.CustomerType;
import com.ifive.premiumsfa.datas.models.responses.TownList;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Syss4 on 2/14/2018.
 */

public class CustListAdapter extends RecyclerView.Adapter<CustListAdapter.MyViewHolder> {
    private Context context;
    private List<CustomerType> cartList;
    private ArrayList<CustomerType> townList;
    Movement movement;
    SessionManager sessionManager;
    Integer townID;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout viewForeground;
        public TextView townName, serialNumber;
        public MyViewHolder(View view) {
            super(view);
            townName = view.findViewById(R.id.cust_names);
            serialNumber = view.findViewById(R.id.serial_numbers);
            viewForeground = view.findViewById(R.id.view_foregrounds);
        }
    }

    public CustListAdapter(Context context, List<CustomerType> cartList, Movement movement, int townID) {
        this.context = context;
        this.cartList = cartList;
        this.movement = movement;
        this.townID = townID;
        this.townList = new ArrayList<CustomerType>();
        this.townList.addAll(cartList);
    }

    @Override
    public CustListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cust_list, parent, false);

        return new CustListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CustListAdapter.MyViewHolder holder, final int position) {
        final CustomerType item = cartList.get(position);
        holder.townName.setText(item.getCustomerType());
        holder.serialNumber.setText(position+1+"");
        holder.viewForeground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                movement.setSaleCustPreference(item.getCustomerTypeId(),item.getCustomerType());
            }
        });
        if((townID != 0) && (townID.equals(item.getCustomerTypeId()))){
            holder.viewForeground.setBackground(context.getResources().getDrawable(R.drawable.green_bg));
            holder.townName.setTextColor(context.getResources().getColor(R.color.white));
        }else{
            holder.viewForeground.setBackground(context.getResources().getDrawable(R.drawable.gradient_bg2));
            holder.townName.setTextColor(context.getResources().getColor(R.color.black_low));
        }
    }

    @Override
    public int getItemCount() {
        return cartList.size();
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        cartList.clear();
        if (charText.length() == 0) {
            cartList.addAll(townList);
        }else{
            for (CustomerType wp : townList){
                if (wp.getCustomerType().toLowerCase(Locale.getDefault()).contains(charText)){
                    cartList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}
