/*
package com.ifive.premiumsfa.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;



import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SaleOrderActivity extends BaseActivity implements RecyclerItemTouchHelper.RecyclerItemTouchHelperListener{

    ProgressDialog pDialog;
    @BindView(R.id.others_text)
    EditText otherText;
    @BindView(R.id.items_data_list)
    RecyclerView itemsDataList;
    @BindView(R.id.total_amount)
    TextView textTotalAmount;
    @BindView(R.id.select_town)
    LinearLayout selectTown;
    @BindView(R.id.select_beat)
    LinearLayout selectBeat;
    @BindView(R.id.select_outlet)
    LinearLayout selectOutlet;
    @BindView(R.id.select_stockist)
    LinearLayout selectStockist;
    @BindView(R.id.town_name)
    TextView sTownName;
    @BindView(R.id.outlet_name)
    TextView sOutletName;
    @BindView(R.id.stockist_name)
    TextView sStockistName;
    @BindView(R.id.beat_name)
    TextView sBeatName;
    @BindView(R.id.reason_spinner)
    Spinner reasonSpinner;
    @BindView(R.id.general_menu)
    LinearLayout generalMenu;
    @BindView(R.id.market_menu)
    LinearLayout marketMenu;
    SessionManager sessionManager;
    private List<SchemeList> schemeLists;
    GeneralItemListAdapter generalItemListAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    double totalAmount;

    //For Alert Dialog BOX
    AlertDialog chartAlertDialog;
    AlertDialog.Builder chartDialog;
    EditText quantity, priceDisplay;
    private List<ProductList> productsList;
    private List<ItemLists> itemPurchaseLists;
    private List<ReasonList> reasonLists;
    private ReasonSpinnerAdapter reasonSpinnerAdapter;
    private SaleOrderRequest itemsListNeeded;
    private CollectionResponse collectionResponse;
    private Typeface typeface;
    ActionBar actionBar;
    int townID=0, stockistID=0, beatID=0, outletID=0;
    private IDName itemData;

    EditText outletName,outletAddress;
    private ReasonList reasonSelected;
    private MarketItemListAdapter marketItemListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sale_order);
        ButterKnife.bind(this);
        pDialog = MukthaEngine.myInstance.getProgDialog(this);
        itemPurchaseLists = new ArrayList<>();
        productsList = new ArrayList<>();
        reasonLists = new ArrayList<>();
        schemeLists = new ArrayList<>();
        sessionManager = new SessionManager();
        typeface = MukthaEngine.myInstance.getCommonTypeFace(this);
        SpannableString titleSpan = new SpannableString("Sales Order");
        titleSpan.setSpan(new CustomTypefaceSpan("" , typeface), 0 ,
                titleSpan.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        actionBar = getSupportActionBar();
        actionBar.setTitle(titleSpan);
        getDatasList();
    }

    public void getDatasList(){
        if (MukthaEngine.isNetworkAvailable(this)) {
            pDialog.show();
            UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);
            DatasRequest listDatasRequest = new DatasRequest();
            listDatasRequest.setListReasons(true);
            listDatasRequest.setListSchemes(true);
            listDatasRequest.setListProducts(true);
            productsList = new ArrayList<>();
            Call<DatasResponse> callEnqueue = userAPICall.listDatas(listDatasRequest);
            callEnqueue.enqueue(new Callback<DatasResponse>() {
                @Override
                public void onResponse(Call<DatasResponse> call, Response<DatasResponse> response) {
                    if(response.body().getListProducts()!=null){
                        productsList.add(getCleanProduct());
                        productsList.addAll(response.body().getListProducts());
                    }
                    if(response.body().getListSchemes()!=null){
                        schemeLists.add(getCleanScheme());
                        schemeLists.addAll(response.body().getListSchemes());
                    }
                    if(response.body().getListReasons()!=null){
                        reasonLists.add(getCleanReason());
                        reasonLists.addAll(response.body().getListReasons());
                    }
                    setItemRecyclerView();
                    setReasonSpinner();
                    pDialog.dismiss();
                    addItemClick();
                    addItemClick();
                    addItemClick();
                }
                @Override
                public void onFailure(Call<DatasResponse> call, Throwable t) {
                    Toast.makeText(SaleOrderActivity.this,  t.getMessage(), Toast.LENGTH_SHORT).show();
                    pDialog.dismiss();
                }
            });
        } else {
            MukthaEngine.myInstance.snackbarNoInternet(this);
        }
    }

    private ReasonList getCleanReason() {
        ReasonList reasonList = new ReasonList();
        reasonList.setOutletReasonId(-1);
        reasonList.setOutletReason("--Select--");
        reasonList.setCmpyId(0);
        reasonList.setCreatedBy(1);
        reasonList.setCreatedOn("");
        reasonList.setLocId(0);
        reasonList.setOrgId(0);
        reasonList.setUpdatedOn("");
        return reasonList;
    }

    private SchemeList getCleanScheme() {
        SchemeList schemeList = new SchemeList();
        schemeList.setSchemeId(-1);
        schemeList.setCmpyId(0);
        schemeList.setCreatedBy(1);
        schemeList.setCreatedOn("");
        schemeList.setSchemeDescription("");
        schemeList.setLocId(0);
        schemeList.setOrgId(0);
        schemeList.setUpdatedOn("");
        schemeList.setSchemeValue(0.0);
        return schemeList;
    }

    private ProductList getCleanProduct() {
        ProductList productOne = new ProductList();
        productOne.setProductId(-1);
        productOne.setCmpyId(0);
        productOne.setCreatedBy(1);
        productOne.setCreatedOn("");
        productOne.setGst(0);
        productOne.setHsnCode("");
        productOne.setLocId(0);
        productOne.setOrgId(0);
        productOne.setPrice(0.00);
        productOne.setProductCode("");
        productOne.setProductDescription("");
        productOne.setUpdatedBy(1);
        productOne.setUpdatedOn("");
        productOne.setProductName("--Select--");
        return productOne;
    }

    private void setReasonSpinner() {
        reasonSpinnerAdapter= new ReasonSpinnerAdapter(this,
                android.R.layout.simple_spinner_item,
                reasonLists);
        reasonSpinner.setAdapter(reasonSpinnerAdapter);
        reasonSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int position, long id) {
                // Here you get the current item (a User object) that is selected by its position
                reasonSelected = reasonSpinnerAdapter.getItem(position);
                if(reasonSelected.getOutletReasonId()==6){
                    otherText.setVisibility(View.VISIBLE);
                }else{
                    otherText.setVisibility(View.GONE);
                }

            }
            @Override
            public void onNothingSelected(AdapterView<?> adapter) {  }
        });
    }

    private void setItemRecyclerView() {
        if(MukthaEngine.myInstance.storeTypeID==1){
            marketMenu.setVisibility(View.VISIBLE);
            generalMenu.setVisibility(View.GONE);
            marketItemListAdapter = new MarketItemListAdapter(this, itemPurchaseLists,this,productsList,schemeLists);
            itemsDataList.setAdapter(marketItemListAdapter);
        }else{
            marketMenu.setVisibility(View.GONE);
            generalMenu.setVisibility(View.VISIBLE);
            generalItemListAdapter = new GeneralItemListAdapter(this, itemPurchaseLists,this,productsList,schemeLists);
            itemsDataList.setAdapter(generalItemListAdapter);
        }
        mLayoutManager = new LinearLayoutManager(this);
        itemsDataList.setLayoutManager(mLayoutManager);
        itemsDataList.setItemAnimator(new DefaultItemAnimator());
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(itemsDataList);
    }

    @OnClick(R.id.add_outlet)
    public void addOutlet(View view) {
        if(beatID!=0){
            View addItemView = LayoutInflater.from(this)
                    .inflate(R.layout.add_outlet, null);
            AlertDialog.Builder chartDialog = new AlertDialog.Builder(this);
            chartDialog.setView(addItemView);
            final AlertDialog chartAlertDialog2 = chartDialog.show();
            outletName =  addItemView.findViewById(R.id.outlet_name);
            outletAddress =  addItemView.findViewById(R.id.outlet_address);
            Button dismissItem =  addItemView.findViewById(R.id.dismiss_item);
            Button addItem =  addItemView.findViewById(R.id.add_item);
            dismissItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    chartAlertDialog2.dismiss();
                }
            });
            addItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(outletName.getText().toString().trim().equals("")){
                        outletName.setError("Please Enter Outlet Name!");
                    }else if(outletAddress.getText().toString().trim().equals("")){
                        outletAddress.setError("Please Enter Outlet Address!");
                    }else {
                        Outlet outlet = new Outlet();
                        outlet.setBeatId(beatID);
                        outlet.setOutletAddress(outletAddress.getText().toString().trim());
                        outlet.setOutletName(outletName.getText().toString().trim());
                        insertOutlet(outlet);
                        chartAlertDialog2.dismiss();
                    }
                }
            });
        }else{
            Toast.makeText(this, "Please Select Beat before adding Outlet", Toast.LENGTH_SHORT).show();
        }
    }

    private void insertOutlet(final Outlet outlet) {
        if (MukthaEngine.isNetworkAvailable(this)) {
            pDialog.show();
            UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);
            Call<OutletResponse> callEnqueue = userAPICall.insertOutlet(sessionManager.getToken(this),outlet);
            callEnqueue.enqueue(new Callback<OutletResponse>() {
                @Override
                public void onResponse(Call<OutletResponse> call, Response<OutletResponse> response) {
                    if(response.body().getOutletId() != null){
                        setSaleOutletPreference(response.body().getOutletId(),outlet.getOutletName());
                    }
                    pDialog.dismiss();
                }
                @Override
                public void onFailure(Call<OutletResponse> call, Throwable t) {
                    Toast.makeText(SaleOrderActivity.this,  t.getMessage(), Toast.LENGTH_SHORT).show();
                    pDialog.dismiss();
                }
            });
        } else {
            MukthaEngine.myInstance.snackbarNoInternet(this);
        }
    }

    @OnClick(R.id.add_item)
    public void addItemClick() {
        if(productsList.get(0)!=null){
            ItemLists itemLists = new ItemLists();
            itemLists.setProductPosition(0);
            itemLists.setSchemePosition(-1);
            itemLists.setSchemeAmount(0);
            itemLists.setHsnCode(productsList.get(0).getHsnCode());
            itemLists.setPrice(productsList.get(0).getPrice());
            itemLists.setProductId(productsList.get(0).getProductId());
            itemLists.setProductCode(productsList.get(0).getProductCode());
            itemLists.setDescription(productsList.get(0).getProductDescription());
            itemLists.setProductName(productsList.get(0).getProductName());
            itemLists.setQuantity(0);
            itemLists.setAddlAmount(0.0);
            itemLists.setGst(productsList.get(0).getGst());
            itemLists.setTotal(productsList.get(0).getPrice());
            itemLists.setSchemeID(-1);
            itemPurchaseLists.add(itemLists);
        }
        calculateTotalAmount();
        setItemRecyclerView();
    }

    public void calculateTotalAmount() {
        totalAmount = 0;
        for (ItemLists itemList :itemPurchaseLists) {
            double totalSubPrice = MukthaEngine.myInstance.calculateProductSubAmount(itemList);
            totalAmount += totalSubPrice;
        }
        textTotalAmount.setText(MukthaEngine.myInstance.formatNumber(totalAmount)+"");
        if(itemPurchaseLists.size() == 0){
            itemsDataList.setVisibility(View.GONE);
        }else{
            itemsDataList.setVisibility(View.VISIBLE);
        }
//        generalItemListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof GeneralItemListAdapter.MyViewHolder) {
            // get the removed item name to display it in snack bar
            String name = itemPurchaseLists.get(viewHolder.getAdapterPosition()).getProductName();
            // backup of removed item for undo purpose
            final ItemLists deletedItem = itemPurchaseLists.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();
            // remove the item from recycler view
            generalItemListAdapter.removeItem(viewHolder.getAdapterPosition());
            // showing snack bar with Undo option
            Snackbar snackbar = Snackbar
                    .make(((Activity)this).findViewById(android.R.id.content),
                            name + " removed from cart!", Snackbar.LENGTH_LONG);
            snackbar.setAction("UNDO", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // undo is selected, restore the deleted item
                    generalItemListAdapter.restoreItem(deletedItem, deletedIndex);
                }
            });
            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();
        }

    }

    public void editItem(final int position) {
        View addItemView = LayoutInflater.from(this)
                .inflate(R.layout.edit_order_layout, null);
        android.app.AlertDialog.Builder chartDialog = new android.app.AlertDialog.Builder(this);
        chartDialog.setView(addItemView);
        final android.app.AlertDialog chartAlertDialog = chartDialog.show();
        quantity =  addItemView.findViewById(R.id.address);
        priceDisplay =  addItemView.findViewById(R.id.price_display);
        quantity.setText(itemPurchaseLists.get(position).getQuantity()+"");
        priceDisplay.setText(itemPurchaseLists.get(position).getQuantity()* itemPurchaseLists.get(position).getPrice()+"");
        Button dismissItem =  addItemView.findViewById(R.id.dismiss_item);
        Button addItem =  addItemView.findViewById(R.id.edit_item);
        dismissItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chartAlertDialog.dismiss();
            }
        });
        addItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(quantity.getText().toString().trim().equals("")){
                    quantity.setError("Please fill this field !");
                }else {
                    editItemPostion(position);
                    chartAlertDialog.dismiss();
                }
            }
        });
        quantity.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                editCalculatePrice(position);
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });
    }

    private void editItemPostion(int position) {
        double productPrice = MukthaEngine.myInstance.convertStringToDouble(quantity.getText().toString().trim())
                * itemPurchaseLists.get(position).getPrice();
        itemPurchaseLists.get(position)
                .setQuantity(MukthaEngine.myInstance.convertStringToInt(quantity.getText().toString().trim()));
        itemPurchaseLists.get(position)
                .setTotal(productPrice);
        calculateTotalAmount();
    }


    public void editCalculatePrice(int position) {
        if(quantity.getText().toString().trim().equals("")){
            priceDisplay.setText("");
        }else{
            double productPrice = MukthaEngine.myInstance.convertStringToDouble(quantity.getText().toString().trim())
                    * itemPurchaseLists.get(position).getPrice();
            priceDisplay.setText(productPrice+"");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setTitles();
    }

    @OnClick(R.id.reset_sale)
    public void resetSale() {
        itemPurchaseLists.clear();
        generalItemListAdapter.notifyDataSetChanged();
        calculateTotalAmount();
    }

    @OnClick(R.id.submit_sale)
    public void submitSale(View view) {
        if(MukthaEngine.isNetworkAvailable(this)){
            if(setTitles()){
                if(checkToSubmit()){
                    postItems();
                }else{
                    if(reasonSelected.getOutletReasonId()==-1){
                        Toast.makeText(this, "Items not found", Toast.LENGTH_SHORT).show();
                    }else{
                        postItems();
                    }
                }
            }else{
                Toast.makeText(this, "Something Missing", Toast.LENGTH_SHORT).show();
            }
        }else{
            MukthaEngine.myInstance.snackbarNoInternet(this);
        }
    }

    private boolean checkToSubmit() {
        for (ItemLists itemCheck: itemPurchaseLists ) {
            if(itemCheck.getProductId() != -1){
                return true;
            }
        }
        return false;
    }

    private void postItems() {
        pDialog.show();
        itemsListNeeded =new SaleOrderRequest();
        itemsListNeeded.setBeatId(beatID);
        itemsListNeeded.setOutletId(outletID);
        itemsListNeeded.setStockistId(stockistID);
        itemsListNeeded.setTownId(townID);
        itemsListNeeded.setReasonId(reasonSelected.getOutletReasonId());
        itemsListNeeded.setTotalAmount(totalAmount);
        itemsListNeeded.setItems(itemPurchaseLists);
        itemsListNeeded.setMargin(MukthaEngine.myInstance.marginSelected);
        itemsListNeeded.setOutletTypeId(MukthaEngine.myInstance.storeTypeID);
        collectionResponse = new CollectionResponse();
        UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);
        Call<CollectionResponse> callEnqueue = userAPICall
                .postItems(sessionManager.getToken(this) ,itemsListNeeded);
        callEnqueue.enqueue(new Callback<CollectionResponse>() {
            @Override
            public void onResponse(Call<CollectionResponse> call, Response<CollectionResponse> response) {
                collectionResponse = response.body();
                if(collectionResponse != null){
                    Toast.makeText(SaleOrderActivity.this,
                            ""+collectionResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    if(collectionResponse.getMessage().equals("SUCCESS")){
                        resetSale();
                        openSalesListActivity();
                    }
                }
                pDialog.dismiss();
            }
            @Override
            public void onFailure(Call<CollectionResponse> call, Throwable t) {
                pDialog.dismiss();
                Toast.makeText(SaleOrderActivity.this,  t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void openSalesListActivity() {
        startActivity(new Intent(SaleOrderActivity.this, SaleListActivity.class));
    }

    public void setSaleTownPreference(int id, String name) {
        dismissAlert();
        sessionManager = new SessionManager();
        sessionManager.setSaleTown(this,id,name);
        townID = id;
        setTitles();
        selectStockist();
    }

    public void setSaleStockistPreference(int id, String name) {
        dismissAlert();
        sessionManager = new SessionManager();
        sessionManager.setStockist(this,id,name);
        stockistID = id;
        setTitles();
        selectBeat();
    }

    public void setSaleBeatPreference(int id, String name) {
        dismissAlert();
        sessionManager = new SessionManager();
        sessionManager.setBeatID(this,id,name);
        beatID = id;
        setTitles();
        selectOutlet();
    }

    public void setSaleOutletPreference(int id, String name) {
        dismissAlert();
        sessionManager = new SessionManager();
        sessionManager.setOutletID(this,id,name);
        outletID = id;
        setTitles();
        setItemRecyclerView();
    }

    private boolean setTitles() {
        boolean status = true;
        itemData = sessionManager.getTownData(this);
        if(itemData.getId()!=0 && itemData.getName()!=null){
            sTownName.setText(itemData.getName()+"");
            townID = itemData.getId();
        }else{
            status =false;
            sTownName.setText("--Select--");
        }
        itemData = sessionManager.getStockistData(this);
        if(itemData.getId()!=0 && itemData.getName()!=null){
            sStockistName.setText(itemData.getName()+"");
            stockistID = itemData.getId();
        }else{
            status =false;
            sStockistName.setText("--Select--");
        }
        itemData = sessionManager.getBeatData(this);
        if(itemData.getId()!=0 && itemData.getName()!=null){
            sBeatName.setText(itemData.getName()+"");
            beatID = itemData.getId();
        }else{
            status =false;
            sBeatName.setText("--Select--");
        }
        itemData = sessionManager.getOutletData(this);
        if(itemData.getId()!=0 && itemData.getName()!=null){
            sOutletName.setText(itemData.getName()+"");
            outletID = itemData.getId();
        }else{
            status =false;
            sOutletName.setText("--Select--");
        }
        return status;
    }

    @OnClick(R.id.select_town)
    public void selectTown(View view) {
        if (MukthaEngine.isNetworkAvailable(this)) {
            pDialog.show();
            UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);
            DatasRequest listDatasRequest = new DatasRequest();
            listDatasRequest.setListTowns(true);
            Call<DatasResponse> callEnqueue = userAPICall.listDatas(listDatasRequest);
            callEnqueue.enqueue(new Callback<DatasResponse>() {
                @Override
                public void onResponse(Call<DatasResponse> call, Response<DatasResponse> response) {
                    if(response.body().getTownLists() != null){
                        getTownList(response.body().getTownLists());
                    }
                    pDialog.dismiss();
                }
                @Override
                public void onFailure(Call<DatasResponse> call, Throwable t) {
                    Toast.makeText(SaleOrderActivity.this,  t.getMessage(), Toast.LENGTH_SHORT).show();
                    pDialog.dismiss();
                }
            });
        } else {
            MukthaEngine.myInstance.snackbarNoInternet(this);
        }
    }

    public void getTownList(final List<TownList> townLists){
        View addItemView = LayoutInflater.from(this)
                .inflate(R.layout.autosearch_recycler, null);
        chartDialog = new AlertDialog.Builder(this);
        chartDialog.setView(addItemView);
        chartAlertDialog = chartDialog.show();
        RecyclerView townsDataList =  addItemView.findViewById(R.id.items_data_list);
        TextView textTitle =  addItemView.findViewById(R.id.text_title);
        final EditText search_type =  addItemView.findViewById(R.id.search_type);
        textTitle.setText("Town List");
        final TownListAdapter itemShowAdapter = new TownListAdapter(this, townLists, this, townID);
        townsDataList.setAdapter(itemShowAdapter);
        mLayoutManager = new LinearLayoutManager(this);
        townsDataList.setLayoutManager(mLayoutManager);
        townsDataList.setItemAnimator(new DefaultItemAnimator());
        townsDataList.addItemDecoration(new DividerDecorator(this));
        search_type.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = search_type.getText().toString().toLowerCase(Locale.getDefault());
                itemShowAdapter.filter(text);
            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        });
    }

    @OnClick(R.id.select_outlet)
    public void selectOutlet() {
        if (beatID != 0) {
            View addItemView = LayoutInflater.from(this)
                    .inflate(R.layout.outlet_type_selection, null);
            chartDialog = new AlertDialog.Builder(this);
            chartDialog.setView(addItemView);
            chartAlertDialog = chartDialog.show();
            TextView generalStores =  addItemView.findViewById(R.id.general_stores);
            TextView superStores =  addItemView.findViewById(R.id.super_stores);
            if(MukthaEngine.myInstance.storeTypeID==1){
                generalStores.setBackground(getResources().getDrawable(R.drawable.gradient_bg2));
                generalStores.setTextColor(getResources().getColor(R.color.black_low));
                superStores.setBackground(getResources().getDrawable(R.drawable.green_bg));
                superStores.setTextColor(getResources().getColor(R.color.white));
            }else{
                generalStores.setBackground(getResources().getDrawable(R.drawable.green_bg));
                generalStores.setTextColor(getResources().getColor(R.color.white));
                superStores.setBackground(getResources().getDrawable(R.drawable.gradient_bg2));
                superStores.setTextColor(getResources().getColor(R.color.black_low));
            }
            generalStores.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MukthaEngine.myInstance.storeTypeID=2;
                    chartAlertDialog.dismiss();
                    chooseOutlet();
                }
            });
            superStores.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MukthaEngine.myInstance.storeTypeID=1;
                    chartAlertDialog.dismiss();
                    chooseOutlet();
                }
            });
        }else{
            Toast.makeText(this, "Please Select Beat ID", Toast.LENGTH_SHORT).show();
        }
    }

    private void chooseOutlet() {
        if (MukthaEngine.isNetworkAvailable(this)) {
            pDialog.show();
            UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);
            DatasRequest listDatasRequest = new DatasRequest();
            listDatasRequest.setOutletTypeId(MukthaEngine.myInstance.storeTypeID);
            listDatasRequest.setBeatId(beatID);
            listDatasRequest.setListOutlets(true);
            Call<DatasResponse> callEnqueue = userAPICall.listDatas(listDatasRequest);
            callEnqueue.enqueue(new Callback<DatasResponse>() {
                @Override
                public void onResponse(Call<DatasResponse> call, Response<DatasResponse> response) {
                    if (response.body().getListOutlets() != null) {
                        getOutletList(response.body().getListOutlets());
                    }
                    pDialog.dismiss();
                }

                @Override
                public void onFailure(Call<DatasResponse> call, Throwable t) {
                    Toast.makeText(SaleOrderActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    pDialog.dismiss();
                }
            });
        } else {
            MukthaEngine.myInstance.snackbarNoInternet(this);
        }

    }

    public void getOutletList(List<OutletList> outletLists){
        View addItemView = LayoutInflater.from(this)
                .inflate(R.layout.autosearch_recycler, null);
        chartDialog = new AlertDialog.Builder(this);
        chartDialog.setView(addItemView);
        chartAlertDialog = chartDialog.show();
        RecyclerView townsDataList =  addItemView.findViewById(R.id.items_data_list);
        TextView textTitle =  addItemView.findViewById(R.id.text_title);
        final EditText search_type =  addItemView.findViewById(R.id.search_type);
        textTitle.setText("Outlet List");
        final OutletListAdapter itemShowAdapter = new OutletListAdapter(this, outletLists,this,outletID);
        townsDataList.setAdapter(itemShowAdapter);
        mLayoutManager = new LinearLayoutManager(this);
        townsDataList.setLayoutManager(mLayoutManager);
        townsDataList.setItemAnimator(new DefaultItemAnimator());
        townsDataList.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        search_type.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = search_type.getText().toString().toLowerCase(Locale.getDefault());
                itemShowAdapter.filter(text);
            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        });
    }

    @OnClick(R.id.select_beat)
    public void selectBeat() {
        if (MukthaEngine.isNetworkAvailable(this)) {
            if (townID != 0) {
                pDialog.show();
                UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);
                DatasRequest listDatasRequest = new DatasRequest();
                listDatasRequest.setTownId(townID);
                listDatasRequest.setListTownBeats(true);
                Call<DatasResponse> callEnqueue = userAPICall.listDatas(listDatasRequest);
                callEnqueue.enqueue(new Callback<DatasResponse>() {
                    @Override
                    public void onResponse(Call<DatasResponse> call, Response<DatasResponse> response) {
                        if (response.body().getListTownBeats() != null) {
                            getBeatList(response.body().getListTownBeats());
                        }
                        pDialog.dismiss();
                    }

                    @Override
                    public void onFailure(Call<DatasResponse> call, Throwable t) {
                        Toast.makeText(SaleOrderActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        pDialog.dismiss();
                    }
                });
            }else{

            }
        } else {
            MukthaEngine.myInstance.snackbarNoInternet(this);
        }
    }

    public void getBeatList(List<TownBeatList> townBeatLists){
        View addItemView = LayoutInflater.from(this)
                .inflate(R.layout.autosearch_recycler, null);
        chartDialog = new AlertDialog.Builder(this);
        chartDialog.setView(addItemView);
        chartAlertDialog = chartDialog.show();
        RecyclerView townsDataList =  addItemView.findViewById(R.id.items_data_list);
        TextView textTitle =  addItemView.findViewById(R.id.text_title);
        final EditText search_type =  addItemView.findViewById(R.id.search_type);
        textTitle.setText("Beat List");
        final BeatListAdapter itemShowAdapter = new BeatListAdapter(this, townBeatLists,this,beatID);
        townsDataList.setAdapter(itemShowAdapter);
        mLayoutManager = new LinearLayoutManager(this);
        townsDataList.setLayoutManager(mLayoutManager);
        townsDataList.setItemAnimator(new DefaultItemAnimator());
        search_type.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = search_type.getText().toString().toLowerCase(Locale.getDefault());
                itemShowAdapter.filter(text);
            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        });
    }

    public void dismissAlert(){
        if(chartAlertDialog != null && chartAlertDialog.isShowing()){
            chartAlertDialog.dismiss();
        }
    }

    @OnClick(R.id.select_stockist)
    public void selectStockist() {
        if (MukthaEngine.isNetworkAvailable(this)) {
            if (townID != 0) {
                pDialog.show();
                UserAPICall userAPICall = RetroFitEngine.getRetrofit().create(UserAPICall.class);
                DatasRequest listDatasRequest = new DatasRequest();
                listDatasRequest.setTownId(townID);
                listDatasRequest.setListTownStockist(true);
                Call<DatasResponse> callEnqueue = userAPICall.listDatas(listDatasRequest);
                callEnqueue.enqueue(new Callback<DatasResponse>() {
                    @Override
                    public void onResponse(Call<DatasResponse> call, Response<DatasResponse> response) {
                        if (response.body().getListTownStockist() != null) {
                            getStockistList(response.body().getListTownStockist());
                        }
                        pDialog.dismiss();
                    }

                    @Override
                    public void onFailure(Call<DatasResponse> call, Throwable t) {
                        Toast.makeText(SaleOrderActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        pDialog.dismiss();
                    }
                });
            }else{

            }
        } else {
            MukthaEngine.myInstance.snackbarNoInternet(this);
        }
    }

    public void getStockistList(List<TownStockistList> townStockistLists){
        View addItemView = LayoutInflater.from(this)
                .inflate(R.layout.autosearch_recycler, null);
        chartDialog = new AlertDialog.Builder(this);
        chartDialog.setView(addItemView);
        chartAlertDialog = chartDialog.show();
        RecyclerView townsDataList =  addItemView.findViewById(R.id.items_data_list);
        TextView textTitle =  addItemView.findViewById(R.id.text_title);
        final EditText search_type =  addItemView.findViewById(R.id.search_type);
        textTitle.setText("Stockist List");
        final StockistListAdapter itemShowAdapter = new StockistListAdapter(this, townStockistLists,this,stockistID);
        townsDataList.setAdapter(itemShowAdapter);
        mLayoutManager = new LinearLayoutManager(this);
        townsDataList.setLayoutManager(mLayoutManager);
        townsDataList.setItemAnimator(new DefaultItemAnimator());
        townsDataList.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        search_type.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
                String text = search_type.getText().toString().toLowerCase(Locale.getDefault());
                itemShowAdapter.filter(text);
            }
            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }
        });
    }


    public void setItemPosition(ProductList item, int pPosition, int position) {
        if(itemPurchaseLists.get(position).getProductId() != item.getProductId()){
            itemPurchaseLists.get(position).setProductPosition(pPosition);
            itemPurchaseLists.get(position).setProductId(item.getProductId());
            itemPurchaseLists.get(position).setHsnCode(item.getHsnCode());
            itemPurchaseLists.get(position).setPrice(item.getPrice());
            itemPurchaseLists.get(position).setProductId(item.getProductId());
            itemPurchaseLists.get(position).setProductCode(item.getProductCode());
            itemPurchaseLists.get(position).setDescription(item.getProductDescription());
            itemPurchaseLists.get(position).setGst(item.getGst());
            itemPurchaseLists.get(position).setProductName(item.getProductName());
            double totalSubPrice = MukthaEngine.myInstance.calculateProductSubAmount(itemPurchaseLists.get(position));
            itemPurchaseLists.get(position).setTotal(totalSubPrice);
            calculateTotalAmount();
        }
    }

    public void setSchemeItemPosition(SchemeList item, int sPosition, int position) {
        if(itemPurchaseLists.get(position).getSchemeID() != item.getSchemeId()) {
            itemPurchaseLists.get(position).setSchemePosition(sPosition);
            itemPurchaseLists.get(position).setSchemeID(item.getSchemeId());
            itemPurchaseLists.get(position).setSchemeAmount(item.getSchemeValue());
            calculateTotalAmount();
        }
    }

    public void setItemQuantity(int qty, int position) {
        itemPurchaseLists.get(position).setQuantity(qty);
        calculateTotalAmount();
    }

    public void setItemAddlAmt(double amount, int position) {
        itemPurchaseLists.get(position).setAddlAmount(amount);
        calculateTotalAmount();
    }
}
*/
